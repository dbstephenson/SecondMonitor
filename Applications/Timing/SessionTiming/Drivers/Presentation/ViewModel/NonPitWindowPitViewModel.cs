﻿namespace SecondMonitor.Timing.SessionTiming.Drivers.Presentation.ViewModel
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using Drivers.ViewModel;
    using Timing.ViewModel;
    using ViewModels;

    public class NoPitWindowPitViewModel : AbstractViewModel<DriverTiming>, IPitStopInfoViewModel
    {
        private string _tyreCompound;
        private bool _isPitStopCountShown;
        private PitInfoBriefDescriptionKind _pitInfoBriefDescription;
        private PitStopCountGeneralizationKind _pitStopCountGeneralization;
        private string _lastPitInfo;
        private string _pitStopCount;

        public string TyreCompound
        {
            get => _tyreCompound;
            set => SetProperty(ref _tyreCompound, value);
        }

        public bool IsPitStopCountShown
        {
            get => _isPitStopCountShown;
            set => SetProperty(ref _isPitStopCountShown, value);
        }

        public string PitStopCount
        {
            get => _pitStopCount;
            set => SetProperty(ref _pitStopCount, value);
        }

        public PitStopCountGeneralizationKind PitStopCountGeneralization
        {
            get => _pitStopCountGeneralization;
            set => SetProperty(ref _pitStopCountGeneralization, value);
        }

        public PitInfoBriefDescriptionKind PitInfoBriefDescription
        {
            get => _pitInfoBriefDescription;
            set => SetProperty(ref _pitInfoBriefDescription, value);
        }
        public string LastPitInfo
        {
            get => _lastPitInfo;
            private set => SetProperty(ref _lastPitInfo, value);
        }

        public override DriverTiming SaveToNewModel()
        {
            throw new System.NotImplementedException();
        }

        public bool CanBeUsed(SimulatorDataSet dataSet)
        {
            return dataSet.SessionInfo.SessionType != SessionType.Race || dataSet.SessionInfo.PitWindow.PitWindowState == PitWindowState.None;
        }

        public void UpdatePitInformation(DriverTiming driverTiming, SimulatorDataSet dataSet)
        {
            TyreCompound = driverTiming.DriverInfo.CarInfo?.WheelsInfo?.FrontLeft?.TyreVisualType ?? string.Empty;
            UpdatePitInfo(driverTiming);
        }

        protected override void ApplyModel(DriverTiming driverTiming)
        {
            TyreCompound = driverTiming.DriverInfo.CarInfo?.WheelsInfo?.FrontLeft?.TyreVisualType ?? string.Empty;
            UpdatePitInfo(driverTiming);
        }

        private void UpdatePitInfo(DriverTiming driverTiming)
        {
            if (driverTiming.DriverInfo.FinishStatus != DriverFinishStatus.None)
            {
                return;
            }
            if (driverTiming.Session.SessionType != SessionType.Race)
            {
                IsPitStopCountShown = false;
                PitInfoBriefDescription = PitInfoBriefDescriptionKind.None;
                LastPitInfo = driverTiming.InPits ? "In Pits" : string.Empty;
                return;
            }

            UpdatePitInfoRace(driverTiming);
        }

        private void UpdatePitInfoRace(DriverTiming driverTiming)
        {
            int pitStopCount = driverTiming.PitCount;
            int playerPitStopCount = driverTiming.Session.Player.PitCount;
            PitStopCount = pitStopCount.ToString();
            int pitStopPlayerDifference = pitStopCount - playerPitStopCount;

            IsPitStopCountShown =true;

            if (pitStopPlayerDifference == 0)
            {
                PitStopCountGeneralization = PitStopCountGeneralizationKind.SameAsPlayer;
            }else if (pitStopPlayerDifference < 0)
            {
                PitStopCountGeneralization = PitStopCountGeneralizationKind.LessThanPlayer;
            }
            else
            {
                PitStopCountGeneralization = PitStopCountGeneralizationKind.MoreThanPLayer;
            }


            if (driverTiming.InPits && driverTiming.LastPitStop != null)
            {
                if (driverTiming.LastPitStop.Phase == PitStopInfo.PitPhase.Entry)
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.PitEntry;
                }else if (driverTiming.LastPitStop.Phase == PitStopInfo.PitPhase.InPits)
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.InPits;
                }
                else
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.PitExit;
                }

                LastPitInfo = driverTiming.LastPitStop.PitInfoFormatted;
                return;
            }

            if (driverTiming.LastPitStop?.EntryLap == null || driverTiming.CurrentLap.LapNumber - driverTiming.LastPitStop.EntryLap.LapNumber > 4)
            {
                int tyreAge = driverTiming.TyresAge;
                int playersTyreAge = (driverTiming.Session?.Player?.TyresAge).GetValueOrDefault();
                int tyreAgeDifference = tyreAge - playersTyreAge;
                if (tyreAgeDifference > 8)
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.TyresOlderThanPlayer;
                }
                else if (tyreAgeDifference < -8)
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.TyresYoungerThanPlayers;
                }
                else
                {
                    PitInfoBriefDescription = PitInfoBriefDescriptionKind.None;
                }
                LastPitInfo = $"{tyreAge}L";
                return;
            }

            if (driverTiming.IsPlayer || driverTiming.Session?.Player?.LastPitStop == null)
            {
                PitInfoBriefDescription = PitInfoBriefDescriptionKind.None;
                LastPitInfo = $"{driverTiming.LastPitStop.PitStopDuration.FormatTimeSpanOnlySecondNoMiliseconds(false)}s";
                return;
            }

            TimeSpan pitStopDifference = driverTiming.LastPitStop.PitStopDuration - driverTiming.Session.Player.LastPitStop.PitStopDuration;
            if (pitStopDifference.TotalSeconds > 1)
            {
                PitInfoBriefDescription = PitInfoBriefDescriptionKind.PitStopSlowerThanPlayer;
            }
            else if (pitStopDifference.TotalSeconds < -1)
            {
                PitInfoBriefDescription = PitInfoBriefDescriptionKind.PitStopFasterThanPlayer;
            }
            else
            {
                PitInfoBriefDescription = PitInfoBriefDescriptionKind.None;
            }
            LastPitInfo = $"{pitStopDifference.FormatTimeSpanOnlySecondNoMiliseconds(true)}s";
        }
    }
}
