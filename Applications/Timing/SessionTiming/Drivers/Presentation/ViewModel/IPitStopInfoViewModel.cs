﻿namespace SecondMonitor.Timing.SessionTiming.Drivers.Presentation.ViewModel
{
    using DataModel.Snapshot;
    using Drivers.ViewModel;
    using ViewModels;

    public interface IPitStopInfoViewModel : IViewModel<DriverTiming>
    {
        bool CanBeUsed(SimulatorDataSet dataSet);

        void UpdatePitInformation(DriverTiming driverTiming, SimulatorDataSet dataSet);
    }
}