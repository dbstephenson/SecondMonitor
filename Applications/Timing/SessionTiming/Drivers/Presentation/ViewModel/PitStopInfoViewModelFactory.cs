﻿namespace SecondMonitor.Timing.SessionTiming.Drivers.Presentation.ViewModel
{
    using DataModel.Snapshot;

    public class PitStopInfoViewModelFactory
    {
        public IPitStopInfoViewModel Create(SimulatorDataSet dataSet)
        {
            return dataSet.SessionInfo.PitWindow.PitWindowState != PitWindowState.None ? new PitWindowPitViewModel() : CreateDefault();
        }

        public IPitStopInfoViewModel CreateDefault()
        {
            return new NoPitWindowPitViewModel();
        }
    }
}