﻿
namespace SecondMonitor.Timing.SessionTiming.ViewModel
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using SecondMonitor.DataModel.Snapshot.Drivers;
    using Drivers;
    using LapTimings;
    using SecondMonitor.Timing.SessionTiming.Drivers.ViewModel;
    using NLog;
    using Rating.Application.Championship;
    using Rating.Application.Rating.RatingProvider;
    using Rating.Common.DataModel.Player;
    using Telemetry;
    using Timing.ViewModel;
    using TrackRecords.Controller;
    using ViewModels.SessionEvents;

    public class SessionTiming
    {
        private readonly ISessionRatingProvider _sessionRatingProvider;
        private readonly ITrackRecordsController _trackRecordsController;
        private readonly IChampionshipCurrentEventPointsProvider _championshipCurrentEventPointsProvider;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly Stopwatch _gapRefreshStopwatch;
        private readonly DriverLapSectorsTrackerFactory _driverLapSectorsTrackerFactory;
        private readonly MapManagementController _mapManagementController;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public event EventHandler<LapEventArgs> LapCompleted;
        public event EventHandler<DriverListModificationEventArgs> DriverAdded;
        public event EventHandler<DriverListModificationEventArgs> DriverRemoved;

        private readonly Stopwatch _ratingUpdateStopwatch;
        private readonly Dictionary<string, BestTimesSetViewModel> _bestTimesForClasses;


        public SessionTiming(TimingApplicationViewModel timingApplicationViewModel, ISessionTelemetryController sessionTelemetryController, ISessionRatingProvider sessionRatingProvider, ITrackRecordsController trackRecordsController, IChampionshipCurrentEventPointsProvider championshipCurrentEventPointsProvider,
            ISessionEventProvider sessionEventProvider, DriverLapSectorsTrackerFactory driverLapSectorsTrackerFactory, SimulatorDataSet initialDataSet, MapManagementController mapManagementController)
        {
            InitializeOneTimeProperties(initialDataSet);
            SessionBestTimesViewModel = new BestTimesSetViewModel();
            _sessionRatingProvider = sessionRatingProvider;
            _trackRecordsController = trackRecordsController;
            _championshipCurrentEventPointsProvider = championshipCurrentEventPointsProvider;
            _sessionEventProvider = sessionEventProvider;
            _driverLapSectorsTrackerFactory = driverLapSectorsTrackerFactory;
            _mapManagementController = mapManagementController;
            TimingApplicationViewModel = timingApplicationViewModel;
            SessionTelemetryController = sessionTelemetryController;
            _ratingUpdateStopwatch = Stopwatch.StartNew();
            _gapRefreshStopwatch = Stopwatch.StartNew();
            GlobalKey = Guid.NewGuid();
            _bestTimesForClasses = new Dictionary<string, BestTimesSetViewModel>();
        }


        public BestTimesSetViewModel SessionBestTimesViewModel { get;}

        public DriverTiming Player { get; private set; }

        public DriverTiming Leader { get; private set; }

        public TimeSpan SessionTime { get; private set; }

        public bool IsMultiClass { get; set; }

        public Guid GlobalKey { get; }

        public double TotalSessionLength { get; private set; }

        public TimeSpan SessionStarTime { get; private set; }

        public bool IsFinished { get; private set; }


        public SessionType SessionType { get; private set; }

        public bool DisplayBindTimeRelative
        {
            get;
            set;
        }

        public bool DisplayGapToPlayerRelative { get; set; }
        public bool WasGreen { get; private set; }

        public TimingApplicationViewModel TimingApplicationViewModel { get; private set; }
        public ISessionTelemetryController SessionTelemetryController { get; }

        public SimulatorDataSet LastSet { get; private set; } = new SimulatorDataSet("None");


        public Dictionary<string, DriverTiming> Drivers { get; private set; }

        public int PaceLaps
        {
            get;
            set;
        }

        public bool RetrieveAlsoInvalidLaps { get; set; }

        public int SessionCompletedPerMiles
        {
            get
            {
                if (LastSet != null && LastSet.SessionInfo.SessionLengthType == SessionLengthType.Laps)
                {
                    return (int)(((LastSet.LeaderInfo.CompletedLaps
                                   + LastSet.LeaderInfo.LapDistance / LastSet.SessionInfo.TrackInfo.LayoutLength.InMeters)
                                  / LastSet.SessionInfo.TotalNumberOfLaps) * 1000);
                }

                if (LastSet != null && (LastSet.SessionInfo.SessionLengthType == SessionLengthType.Time || LastSet.SessionInfo.SessionLengthType == SessionLengthType.TimeWithExtraLap))
                {
                    return (int)(1000 - (LastSet.SessionInfo.SessionTimeRemaining / TotalSessionLength) * 1000);
                }

                return 0;
            }
        }

        public BestTimesSetViewModel GetBestTimesForClass(string classId)
        {
            if (_bestTimesForClasses.TryGetValue(classId, out BestTimesSetViewModel bestTimesSetViewModel))
            {
                return bestTimesSetViewModel;
            }

            bestTimesSetViewModel = new BestTimesSetViewModel(classId);
            _bestTimesForClasses.Add(classId, bestTimesSetViewModel);
            return bestTimesSetViewModel;
        }

        public void Finish()
        {
            IsFinished = true;
        }

        private void DriverOnLapCompleted(object sender, LapEventArgs lapEventArgs)
        {
            LapCompleted?.Invoke(this, lapEventArgs);

            if (lapEventArgs.Lap.Driver.IsPlayer)
            {
                _mapManagementController.OnLapCompleted(lapEventArgs);
            }

            if (lapEventArgs.Lap.Driver.IsPlayer && TimingApplicationViewModel.DisplaySettingsViewModel.TelemetrySettingsViewModel.IsTelemetryLoggingEnabled && (lapEventArgs.Lap.Valid || TimingApplicationViewModel.DisplaySettingsViewModel.TelemetrySettingsViewModel.LogInvalidLaps))
            {
                SessionTelemetryController.TrySaveLapTelemetry(lapEventArgs.Lap);
            }

            if (!lapEventArgs.Lap.Valid)
            {
                Player.IsLastLapTrackRecord = false;
                return;
            }

            SessionBestTimesViewModel.CheckAndUpdateBestLap(lapEventArgs.Lap);
            GetBestTimesForClass(lapEventArgs.Lap.Driver.CarClassId).CheckAndUpdateBestLap(lapEventArgs.Lap);

            if (lapEventArgs.Lap.Driver.IsPlayer && Player != null)
            {
                Player.IsLastLapTrackRecord = _trackRecordsController.EvaluateFastestLapCandidate(lapEventArgs.Lap);
            }
        }

        private void OnSectorCompletedEvent(object sender, LapInfo.SectorCompletedArgs e)
        {
            SectorTiming completedSector = e.SectorTiming;
            if (!e.SectorTiming.Lap.Valid)
            {
                return;
            }

            SessionBestTimesViewModel.UpdateSectorBest(completedSector);
            GetBestTimesForClass(completedSector.Lap.Driver.CarClassId).UpdateSectorBest(completedSector);
        }

        private void AddNewDriver(SimulatorDataSet dataSet, DriverInfo newDriverInfo)
        {
            if (Drivers.TryGetValue(newDriverInfo.DriverSessionId, out DriverTiming driverTiming))
            {
                if (driverTiming.IsActive)
                {
                    return;
                }

                driverTiming.IsActive = true;

                if (_sessionRatingProvider.TryGetRatingForDriverCurrentSession(newDriverInfo.DriverSessionId, out DriversRating driversRatingNew))
                {
                    driverTiming.Rating = driversRatingNew.Rating;
                }

                if (_championshipCurrentEventPointsProvider.TryGetPointsForDriver(newDriverInfo.DriverLongName, out int pointsNew))
                {
                    driverTiming.ChampionshipPoints = pointsNew;
                }
                Logger.Info($"Driver Reactivated, {driverTiming.DriverId}");
                RaiseDriverAddedEvent(dataSet, driverTiming);
                return;

            }

            Logger.Info($"Adding new driver: {newDriverInfo.DriverSessionId}");
            DriverTiming newDriver = DriverTiming.FromModel(newDriverInfo, this, _driverLapSectorsTrackerFactory, SessionType != SessionType.Race);
            newDriver.SectorCompletedEvent += OnSectorCompletedEvent;
            newDriver.LapInvalidated += LapInvalidatedHandler;
            newDriver.LapCompleted += DriverOnLapCompleted;
            newDriver.LapTimeReevaluated += DriverOnLapTimeReevaluated;
            Drivers.Add(newDriver.DriverId, newDriver);

            if (_sessionRatingProvider.TryGetRatingForDriverCurrentSession(newDriverInfo.DriverSessionId, out DriversRating driversRating))
            {
                newDriver.Rating = driversRating.Rating;
            }

            if (_championshipCurrentEventPointsProvider.TryGetPointsForDriver(newDriverInfo.DriverLongName, out int points))
            {
                newDriver.ChampionshipPoints = points;
            }
            RaiseDriverAddedEvent(dataSet, newDriver);
            Logger.Info($"Added new driver");
        }

        private void DriverOnLapTimeReevaluated(object sender, LapEventArgs e)
        {
            SessionBestTimesViewModel.FindBestLap(Drivers.Values);
            GetBestTimesForClass(e.Lap.Driver.CarClassId).FindBestLap(Drivers.Values.Where(x => x.CarClassId == e.Lap.Driver.CarClassId).ToList());
        }

        private void LapInvalidatedHandler(object sender, LapEventArgs e)
        {
            SessionBestTimesViewModel.InvalidateBestSectorForLap(e.Lap, Drivers.Values);
            GetBestTimesForClass(e.Lap.Driver.CarClassId).InvalidateBestSectorForLap(e.Lap, Drivers.Values.Where(x => x.CarClassId == e.Lap.Driver.CarClassId).ToList());
        }

        public void UpdateTiming(SimulatorDataSet dataSet)
        {
            LastSet = dataSet;
            SessionTime = dataSet.SessionInfo.SessionTime - SessionStarTime;
            SessionType = dataSet.SessionInfo.SessionType;
            WasGreen |= dataSet.SessionInfo.SessionPhase == SessionPhase.Green;
            IsMultiClass |= dataSet.SessionInfo.IsMultiClass;
            UpdateDrivers(dataSet);

        }

        private void UpdateDrivers(SimulatorDataSet dataSet)
        {
            try
            {
                bool updateRating = _ratingUpdateStopwatch.ElapsedMilliseconds > 1000;
                if (updateRating)
                {
                    _ratingUpdateStopwatch.Restart();
                }
                HashSet<string> updatedDrivers = new HashSet<string>();
                foreach (DriverInfo driverInfo in dataSet.DriversInfo)
                {
                    updatedDrivers.Add(driverInfo.DriverSessionId);
                    if (Drivers.TryGetValue(driverInfo.DriverSessionId, out DriverTiming driverToUpdate) && driverToUpdate.IsActive)
                    {
                        driverToUpdate = Drivers[driverInfo.DriverSessionId];
                        UpdateDriver(driverInfo, driverToUpdate, dataSet, updateRating);

                        if (driverToUpdate.IsPlayer)
                        {
                            Player = driverToUpdate;
                        }
                    }
                    else
                    {
                        AddNewDriver(dataSet, driverInfo);
                    }
                }

                List<string> driversToRemove = Drivers.Keys.Where(s => !updatedDrivers.Contains(s) && Drivers[s].IsActive).ToList();

                foreach (var driver in driversToRemove)
                {
                    Logger.Info($"Removing driver {Drivers[driver].DriverId}");
                    RaiseDriverRemovedEvent(dataSet, Drivers[driver]);
                    Drivers[driver].IsActive = false;
                    Logger.Info($"Driver Removed");
                }

                if (_gapRefreshStopwatch.ElapsedMilliseconds > 1000)
                {
                    _gapRefreshStopwatch.Restart();
                    Drivers.Values.ForEach(x => x.CalculateGapToPLayer());
                }


            }
            catch (KeyNotFoundException ex)
            {
                throw new DriverNotFoundException("Driver not found", ex);
            }
        }

        public void SetDrivers(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> drivers)
        {
            foreach (DriverTiming newDriver in drivers)
            {
                newDriver.SectorCompletedEvent += OnSectorCompletedEvent;
                newDriver.LapInvalidated += LapInvalidatedHandler;
                newDriver.LapCompleted += DriverOnLapCompleted;
                newDriver.LapTimeReevaluated += DriverOnLapTimeReevaluated;
                if (newDriver.IsPlayer)
                {
                    Player = newDriver;
                }
            }

            Drivers = drivers.ToDictionary(x => x.DriverId, x => x);
            _sessionEventProvider.NotifyDriversAdded(dataSet, drivers.Select( x=> x.DriverInfo));
        }

        private void RaiseDriverAddedEvent(SimulatorDataSet dataSet, DriverTiming driver)
        {
            _sessionEventProvider.NotifyDriversAdded(dataSet, new[] { driver.DriverInfo });
            DriverAdded?.Invoke(this, new DriverListModificationEventArgs(driver));
        }

        private void RaiseDriverRemovedEvent(SimulatorDataSet dataSet, DriverTiming driver)
        {
            _sessionEventProvider.NotifyDriversRemoved(dataSet, new[] { driver.DriverInfo });
            DriverRemoved?.Invoke(this, new DriverListModificationEventArgs(driver));
        }

        private void UpdateDriver(DriverInfo modelInfo, DriverTiming timingInfo, SimulatorDataSet set, bool updateRating)
        {
            timingInfo.UpdateDriverInfo(modelInfo, set);

            if (timingInfo.Position == 1)
            {
                Leader = timingInfo;
            }

            timingInfo.UpdateLaps(set);

            if (updateRating && timingInfo.Rating == 0 && _sessionRatingProvider.TryGetRatingForDriverCurrentSession(modelInfo.DriverSessionId, out DriversRating driversRating))
            {
                timingInfo.Rating = driversRating.Rating;
            }

            if (updateRating && timingInfo.ChampionshipPoints == 0 && _championshipCurrentEventPointsProvider.TryGetPointsForDriver(modelInfo.DriverLongName, out int points))
            {
                timingInfo.ChampionshipPoints = points;
            }
        }

        private void InitializeOneTimeProperties(SimulatorDataSet initialDataSet)
        {
            SessionStarTime = initialDataSet.SessionInfo.SessionTime;
            SessionType = initialDataSet.SessionInfo.SessionType;
            RetrieveAlsoInvalidLaps = initialDataSet.SessionInfo.SessionType == SessionType.Race;
            if (initialDataSet.SessionInfo.SessionLengthType == SessionLengthType.Time || initialDataSet.SessionInfo.SessionLengthType == SessionLengthType.TimeWithExtraLap)
            {
                TotalSessionLength = initialDataSet.SessionInfo.SessionTimeRemaining;
            }

            PaceLaps = 4;
            DisplayBindTimeRelative = false;
        }

        public IEnumerator GetEnumerator()
        {
            return Drivers.Values.GetEnumerator();
        }
    }
}
