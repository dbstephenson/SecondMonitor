﻿using SecondMonitor.SimdataManagement.SimSettings;

namespace SecondMonitor.Timing.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using Contracts.Commands;
    using DataModel.Snapshot;
    using PluginManager.Core;
    using PluginManager.GameConnector;
    using Presentation.View;
    using SecondMonitor.Telemetry.TelemetryApplication.Controllers;
    using SimdataManagement.DriverPresentation;
    using TelemetryPresentation.MainWindow;
    using ViewModels.Settings.ViewModel;
    using System.Windows;
    using Contracts.NInject;
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using LapTimings.ViewModel;
    using Ninject.Syntax;
    using NLog;
    using PitBoard.Controller;
    using Rating.Application.Championship.Controller;
    using Rating.Application.Rating.Controller;
    using Rating.Application.Rating.RatingProvider.FieldRatingProvider.ReferenceRatingProviders;
    using ReportCreation.ViewModel;
    using SimdataManagement.RaceSuggestion;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.FuelConsumption;
    using ViewModels.Settings;
    using ViewModels.Settings.Model;
    using ViewModels.SimulatorContent;
    using ViewModels.SplashScreen;
    using ViewModels.Track.SituationOverview.Controller;

    public class TimingApplicationController : ISecondMonitorPlugin
    {
        private static readonly string SettingsPath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "SecondMonitor\\settings.json");

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly KernelWrapper _kernelWrapper;
        private TimingApplicationViewModel _timingApplicationViewModel;
        private SimSettingController _simSettingController;
        private PluginsManager _pluginsManager;
        private TimingGui _timingGui;
        private DisplaySettingsViewModel _displaySettingsViewModel;
        private readonly DriverPresentationsManager _driverPresentationsManager;
        private readonly DisplaySettingsLoader _displaySettingsLoader;
        private ReportsController _reportsController;
        private readonly IRatingApplicationController _ratingApplicationController;
        private readonly ISettingsProvider _settingsProvider;
        private readonly ISimulatorContentController _simulatorContentController;
        private readonly ITrackRecordsController _trackRecordsController;
        private readonly IChampionshipController _championshipController;
        private readonly ISessionEventsController _sessionEventsController;
        private readonly IRaceSuggestionController _raceSuggestionController;
        private readonly FuelConsumptionController _fuelConsumptionController;
        private readonly SituationOverviewController _situationOverviewController;
        private readonly DriverLapsWindowManager _driverLapsWindowManager;
        private readonly SettingsWindowController _settingsWindowController;
        private readonly ForceSingleClassVisitor _forceSingleClassVisitor;

        private readonly PitBoardController _pitBoardController;
        private Window _splashScreen;
        private Guid _lastReportedSessionGuid;

        public TimingApplicationController()
        {
            _kernelWrapper = new KernelWrapper();
            _displaySettingsLoader = new DisplaySettingsLoader();
            _ratingApplicationController = _kernelWrapper.Get<IRatingApplicationController>();
            _settingsProvider = _kernelWrapper.Get<ISettingsProvider>();
            _simulatorContentController = _kernelWrapper.Get<ISimulatorContentController>();
            _trackRecordsController = _kernelWrapper.Get<ITrackRecordsController>();
            _championshipController = _kernelWrapper.Get<IChampionshipController>();
            _sessionEventsController = _kernelWrapper.Get<ISessionEventsController>();
            _situationOverviewController = _kernelWrapper.Get<SituationOverviewController>();
            _pitBoardController = _kernelWrapper.Get<PitBoardController>();
            _driverPresentationsManager = _kernelWrapper.Get<DriverPresentationsManager>();
            _fuelConsumptionController = _kernelWrapper.Get<FuelConsumptionController>();
            _driverLapsWindowManager = _kernelWrapper.Get<DriverLapsWindowManager>();
            _settingsWindowController = _kernelWrapper.Get<SettingsWindowController>();
            _forceSingleClassVisitor = _kernelWrapper.Get<ForceSingleClassVisitor>();
            _raceSuggestionController = _kernelWrapper.Get<IRaceSuggestionController>();
        }

        public PluginsManager PluginManager
        {
            get => _pluginsManager;
            set
            {
                _pluginsManager = value;
                _pluginsManager.DataLoaded += OnDataLoaded;
                _pluginsManager.SessionStarted += OnSessionStarted;
                _pluginsManager.DisplayMessage += DisplayMessage;
            }
        }

        public bool IsDaemon => false;

        public string PluginName => "Timing UI";

        public bool IsEnabledByDefault => true;


        public async Task RunPlugin()
        {
            try
            {
                //This is because windows defender is stupid
                ResourceDictionary telemetryResource = new ResourceDictionary {Source = new Uri("pack://application:,,,/TelemetryPresentation;component/TelemetryPresentationTemplates.xaml")};
                Application.Current.Resources.MergedDictionaries.Add(telemetryResource);

                CreateDisplaySettingsViewModel();
                ApplyCustomResources();
                ShowSplashScreen();
                CreateReportsController();
                CreateSimSettingsController();
                await StartControllers();
                _timingApplicationViewModel = _kernelWrapper.Get<TimingApplicationViewModel>();
                _timingApplicationViewModel.PlayerFinished += TimingApplicationViewModelOnPlayerFinished;
                _timingApplicationViewModel.SessionCompleted += TimingApplicationViewModelOnSessionCompleted;
                _timingApplicationViewModel.RatingApplicationViewModel = _ratingApplicationController.RatingApplicationViewModel;
                _timingApplicationViewModel.SidebarViewModel.ChampionshipIconStateViewModel = _championshipController.ChampionshipIconStateViewModel;
                _timingApplicationViewModel.FuelOverviewViewModel = _fuelConsumptionController.FuelOverviewViewModel;
                _timingApplicationViewModel.SuggestionsViewModel = _raceSuggestionController.SuggestionsViewModel;
                BindCommands();
                CreateGui();
                _timingApplicationViewModel?.Initialize();
                await _simulatorContentController.StartControllerAsync();
                await _trackRecordsController.StartControllerAsync();

                HideSplashScreen();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void HideSplashScreen()
        {
            _splashScreen.Close();
        }

        private void ShowSplashScreen()
        {
            var splashScreenViewModel = _kernelWrapper.Get<SplashScreenViewModel>();
            splashScreenViewModel.PrimaryInformation = "Loading...";
            _splashScreen = new Window
            {
                Content = splashScreenViewModel,
                Title = "Starting",
                SizeToContent = SizeToContent.Manual,
                WindowStartupLocation = WindowStartupLocation.Manual,
                ShowInTaskbar = false,
            };

            _splashScreen.Show();

            if (_displaySettingsViewModel?.WindowLocationSetting == null)
            {
                return;
            }

            _splashScreen.WindowStartupLocation = WindowStartupLocation.Manual;
            _splashScreen.Left = _displaySettingsViewModel.WindowLocationSetting.Left;
            _splashScreen.Top = _displaySettingsViewModel.WindowLocationSetting.Top;
            _splashScreen.WindowState = WindowState.Normal;
            _splashScreen.WindowState = (WindowState)_displaySettingsViewModel.WindowLocationSetting.WindowState;
            if (_splashScreen.WindowState == WindowState.Maximized)
            {
                _splashScreen.WindowStyle = WindowStyle.None;
            }
        }

        private async Task StartControllers()
        {
            await _situationOverviewController.StartControllerAsync();
            await _ratingApplicationController.StartControllerAsync();
            await _championshipController.StartControllerAsync();
            await _sessionEventsController.StartControllerAsync();
            await _pitBoardController.StartControllerAsync();
            await _fuelConsumptionController.StartControllerAsync();
            await _settingsWindowController.StartControllerAsync();
            await _raceSuggestionController.StartControllerAsync();
        }

        private Task StopChildControllers()
        {
            return Task.WhenAll(new[]
            {
                _ratingApplicationController.StopControllerAsync(),
                _championshipController.StopControllerAsync(),
                _sessionEventsController.StopControllerAsync(),
                _pitBoardController.StopControllerAsync(),
                _situationOverviewController.StopControllerAsync(),
                _fuelConsumptionController.StopControllerAsync(),
                _settingsWindowController.StopControllerAsync(),
                _raceSuggestionController.StartControllerAsync(),
        });
        }

        private void CreateReportsController()
        {
            _reportsController = new ReportsController(_displaySettingsViewModel);
        }

        private void DisplayMessage(object sender, MessageArgs e)
        {
            _timingApplicationViewModel?.DisplayMessage(e);
        }

        private void OnSessionStarted(object sender, DataEventArgs e)
        {
            _sessionEventsController.Reset();
            _forceSingleClassVisitor.Reset();
            _forceSingleClassVisitor.Visit(e.Data);
            _sessionEventsController.Visit(e.Data);
            _ratingApplicationController.NotifyDataLoaded(e.Data);
            _trackRecordsController.OnSessionStarted(e.Data);
            _timingApplicationViewModel?.StartNewSession(e.Data);
            _fuelConsumptionController.Reset();
        }

        private async void OnDataLoaded(object sender, DataEventArgs e)
        {
            SimulatorDataSet dataSet = e.Data;
            try
            {
                _forceSingleClassVisitor.Visit(dataSet);
                _simSettingController?.ApplySimSettings(dataSet);
                _simulatorContentController.Visit(dataSet);
                _sessionEventsController.Visit(dataSet);
                _timingApplicationViewModel.ApplyDateSet(dataSet);
                _fuelConsumptionController.ApplyDataSet(dataSet);
                await _ratingApplicationController.NotifyDataLoaded(dataSet);

            }
            catch (SimSettingsException ex)
            {
                _timingApplicationViewModel.DisplayMessage(new MessageArgs(ex.Message));
                _simSettingController?.ApplySimSettings(dataSet);
            }

        }

        private void CreateSimSettingsController()
        {
            _simSettingController = new SimSettingController(_displaySettingsViewModel, _kernelWrapper.Get<ICarSpecificationProvider>(), _kernelWrapper.Get<IResolutionRoot>());
        }

        private void CreateGui()
        {
            _timingGui = new TimingGui(_displaySettingsViewModel.IsHwAccelerationEnabled);
            _timingGui.Show();
            _timingGui.Closing += OnGuiClosed;

            if (_displaySettingsViewModel?.WindowLocationSetting != null)
            {
                _timingGui.WindowStartupLocation = WindowStartupLocation.Manual;
                _timingGui.Left = _displaySettingsViewModel.WindowLocationSetting.Left;
                _timingGui.Top = _displaySettingsViewModel.WindowLocationSetting.Top;
                _timingGui.WindowState = WindowState.Normal;
                _timingGui.WindowState = (WindowState)_displaySettingsViewModel.WindowLocationSetting.WindowState;
                if (_timingGui.WindowState == WindowState.Maximized)
                {
                    _timingGui.WindowStyle = WindowStyle.None;
                }
            }

            _timingGui.DataContext = _timingApplicationViewModel;
            Application.Current.MainWindow = _timingGui;
            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
        }

        private async void OnGuiClosed(object sender, CancelEventArgs e)
        {
            try
            {
                Task stopTask = StopChildControllers();
                _displaySettingsViewModel.WindowLocationSetting = new WindowLocationSetting()
                {
                    Left = _timingGui.Left,
                    Top = _timingGui.Top,
                    WindowState = (int) _timingGui.WindowState
                };
                List<Exception> exceptions = new List<Exception>();
                _driverPresentationsManager.SavePresentations();
                _timingApplicationViewModel.SessionCompleted -= TimingApplicationViewModelOnSessionCompleted;
                _timingApplicationViewModel?.TerminatePeriodicTask(exceptions);
                _displaySettingsLoader.TrySaveDisplaySettings(_displaySettingsViewModel.SaveToNewModel(), SettingsPath);
                await Task.WhenAll(stopTask,
                    _trackRecordsController.StopControllerAsync(),
                    _simulatorContentController.StopControllerAsync());
                await _pluginsManager.DeletePlugin(this, exceptions);

            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        private void BindCommands()
        {
            _timingApplicationViewModel.SidebarViewModel.OpenSettingsCommand = new RelayCommand(OpenSettingsWindow);
            _timingApplicationViewModel.SidebarViewModel.OpenCarSettingsCommand = new RelayCommand(OpenCarSettingsWindow);
            _timingApplicationViewModel.SidebarViewModel.OpenCurrentTelemetrySession = new AsyncCommand(OpenCurrentTelemetrySession);
            _timingApplicationViewModel.SidebarViewModel.OpenLastReportCommand = new RelayCommand(_reportsController.OpenLastReport);
            _timingApplicationViewModel.SidebarViewModel.OpenReportFolderCommand = new RelayCommand(_reportsController.OpenReportsFolder);
            _timingApplicationViewModel.SidebarViewModel.OpenChampionshipWindowCommand = new RelayCommand(_championshipController.OpenChampionshipWindow);
            _timingApplicationViewModel.SidebarViewModel.OpenRandomSuggestionsCommand = new RelayCommand(_raceSuggestionController.ToggleRaceSuggestionViewVisibility);
            _timingApplicationViewModel.TimingDataGridViewModel.OpenSelectedDriversLapsCommand = new RelayCommand(OpenSelectedDriversLaps);
        }

        private void OpenSelectedDriversLaps()
        {
            _driverLapsWindowManager.OpenWindowDefault(_timingApplicationViewModel.TimingDataGridViewModel.SelectedDriverTimingViewModel);
        }

        private void CreateDisplaySettingsViewModel()
        {
            _displaySettingsViewModel = _settingsProvider.DisplaySettingsViewModel;
            FillDisplaySettingsOptions();
        }

        private void FillDisplaySettingsOptions()
        {
            _displaySettingsViewModel.RatingSettingsViewModel.AvailableReferenceRatingProviders = _kernelWrapper.Get<IReferenceRatingProviderFactory>().GetAvailableReferenceRatingsProviders();
        }

        private void OpenSettingsWindow()
        {
            _settingsWindowController.OpenSettingWindow();
        }

        private static async Task OpenCurrentTelemetrySession()
        {
            MainWindow mainWindow = new MainWindow();
            TelemetryApplicationController controller = new TelemetryApplicationController(mainWindow);
            await controller.StartControllerAsync();
            mainWindow.Closed += async (sender, args) =>
            {
                await controller.StopControllerAsync();
            };
            await controller.OpenLastSessionFromRepository();
        }

        private void OpenCarSettingsWindow()
        {
            _simSettingController.OpenCarSettingsControl(_timingGui);
        }

        private void TimingApplicationViewModelOnPlayerFinished(object sender, SessionSummaryEventArgs e)
        {
            if (e.Summary.SessionType == SessionType.Race && e.Summary.Drivers.Any(x => x.IsPlayer && x.Finished) && _lastReportedSessionGuid != e.Summary.SessionGuid)
            {
                _lastReportedSessionGuid = e.Summary.SessionGuid;
                _reportsController?.CreateReport(e.Summary);
            }

            if (e.Summary.SessionType == SessionType.Race)
            {
                _ratingApplicationController.OnPlayerFinished(e.Summary);
            }
        }

        private void ApplyCustomResources()
        {
            try
            {
                string sourcePath = Path.Combine(GetAssemblyDirectory(), "Colors.xaml");
                string destinationPath = Path.Combine(_displaySettingsViewModel.ReportingSettingsView.ExportDirectoryReplacedSpecialDirs, "colors.xaml");
                File.Copy(sourcePath, destinationPath, true);

                if (string.IsNullOrWhiteSpace(_displaySettingsViewModel.CustomResourcesPath) || !File.Exists(_displaySettingsViewModel.CustomResourcesPath))
                {
                    return;
                }

                var uri = new Uri(_displaySettingsViewModel.CustomResourcesPath);
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = uri });
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

        }

        private static string GetAssemblyDirectory()
        {
            string path = Assembly.GetEntryAssembly().Location;
            return Path.GetDirectoryName(path);
        }

        private async void TimingApplicationViewModelOnSessionCompleted(object sender, SessionSummaryEventArgs e)
        {
            await _ratingApplicationController.NotifySessionCompletion(e.Summary);
            if (_lastReportedSessionGuid != e.Summary.SessionGuid)
            {
                _lastReportedSessionGuid = e.Summary.SessionGuid;
                _reportsController?.CreateReport(e.Summary);
            }
        }
    }
}