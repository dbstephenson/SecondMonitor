﻿namespace SecondMonitor.Timing.LapTimings.ViewModel
{
    using System;
    using System.Threading.Tasks;
    using DataModel.BasicProperties;
    using SecondMonitor.Timing.SessionTiming.Drivers.ViewModel;
    using Timing.ViewModel;
    using ViewModels;

    public class LapViewModel : AbstractViewModel
    {
        private bool _refresh = true;

        public LapViewModel(ILapInfo lapInfo)
        {
            LapInfo = lapInfo;
            RefreshInfo();
            TimerMethod(RefreshInfo, () => LapInfo.Driver.Session.TimingApplicationViewModel.DisplaySettingsViewModel.RefreshRate);

        }

        public void StopRefresh()
        {
            _refresh = false;
        }

        public ILapInfo LapInfo
        {
            get;
            set;
        }

        private int _lapNumber;
        public int LapNumber
        {
            get => _lapNumber;
            set => SetProperty(ref _lapNumber, value);
        }

        private string _sector1;
        public string Sector1
        {
            get => _sector1;
            set => SetProperty(ref _sector1, value);
        }

        private string _sector2;
        public string Sector2
        {
            get => _sector2;
            set => SetProperty(ref _sector2, value);
        }

        private string _sector3;
        public string Sector3
        {
            get => _sector3;
            set => SetProperty(ref _sector3, value);
        }


        private string _lapTime;
        public string LapTime
        {
            get => _lapTime;
            set => SetProperty(ref _lapTime, value);
        }

        private bool _isSector1PersonalBest;
        public bool IsSector1PersonalBest
        {
            get => _isSector1PersonalBest;
            set => SetProperty(ref _isSector1PersonalBest, value);
        }

        private bool _isSector2PersonalBest;
        public bool IsSector2PersonalBest
        {
            get => _isSector2PersonalBest;
            set => SetProperty(ref _isSector2PersonalBest, value);
        }

        private bool _isSector3PersonalBest;
        public bool IsSector3PersonalBest
        {
            get => _isSector3PersonalBest;
            set => SetProperty(ref _isSector3PersonalBest, value);
        }

        private bool _isSector1SessionBest;
        public bool IsSector1SessionBest
        {
            get => _isSector1SessionBest;
            set => SetProperty(ref _isSector1SessionBest, value);
        }

        private bool _isSector2SessionBest;
        public bool IsSector2SessionBest
        {
            get => _isSector2SessionBest;
            set => SetProperty(ref _isSector2SessionBest, value);
        }

        private bool _isSector3SessionBest;
        public bool IsSector3SessionBest
        {
            get => _isSector3SessionBest;
            set => SetProperty(ref _isSector3SessionBest, value);
        }

        private bool _isLapBestSessionLap;

        public bool IsLapBestSessionLap
        {
            get => _isLapBestSessionLap;
            set => SetProperty(ref _isLapBestSessionLap, value);
        }

        private bool _isLapBestPersonalLap;

        public bool IsLapBestPersonalLap
        {
            get => _isLapBestPersonalLap;
            set => SetProperty(ref _isLapBestPersonalLap, value);
        }


        private bool _isLapValid;
        public bool IsLapValid
        {
            get => _isLapValid;
            set => SetProperty(ref _isLapValid, value);
        }


        private async void TimerMethod(Action timedAction, Func<int> delayAction)
        {
            while (_refresh)
            {
                await Task.Delay(delayAction() * 2);
                timedAction();
            }
        }

        private void RefreshInfo()
        {
            LapNumber = LapInfo.LapNumber;
            Sector1 = GetSector1();
            Sector2 = GetSector2();
            Sector3 = GetSector3();
            LapTime = GetLapTime();
            IsSector1SessionBest = GetIsSector1SessionBest();
            IsSector2SessionBest = GetIsSector2SessionBest();
            IsSector3SessionBest = GetIsSector3SessionBest();
            IsSector1PersonalBest = GetIsSector1PersonalBest();
            IsSector2PersonalBest = GetIsSector2PersonalBest();
            IsSector3PersonalBest = GetIsSector3PersonalBest();
            IsLapBestSessionLap = GetIsLapBestSessionLap();
            IsLapBestPersonalLap = GetIsLapBestPersonalLap();
            IsLapValid = LapInfo.Valid;
        }

        private bool GetIsLapBestSessionLap()
        {
            return LapInfo == LapInfo.Driver.Session.SessionBestTimesViewModel.BestLap;
        }

        private bool GetIsLapBestPersonalLap()
        {
            return LapInfo == LapInfo.Driver.BestLap;
        }

        private bool GetIsSector1SessionBest()
        {
            var sector = LapInfo.Sector1;
            return sector != null && sector == LapInfo.Driver.Session.SessionBestTimesViewModel.BestSector1;
        }

        private bool GetIsSector2SessionBest()
        {
            var sector = LapInfo.Sector2;
            return sector != null && sector == LapInfo.Driver.Session.SessionBestTimesViewModel.BestSector2;
        }

        private bool GetIsSector3SessionBest()
        {
            var sector = LapInfo.Sector3;
            return sector != null && sector == LapInfo.Driver.Session.SessionBestTimesViewModel.BestSector3;
        }

        private bool GetIsSector1PersonalBest()
        {
            var sector = LapInfo.Sector1;
            return sector != null && sector == LapInfo.Driver.BestSector1;
        }

        private bool GetIsSector2PersonalBest()
        {
            var sector = LapInfo.Sector2;
            return sector != null && sector == LapInfo.Driver.BestSector2;
        }

        private bool GetIsSector3PersonalBest()
        {
            var sector = LapInfo.Sector3;
            return sector != null && sector == LapInfo.Driver.BestSector3;
        }

        private string GetSectorTiming(SectorTiming sectorTiming)
        {
            if (sectorTiming == null)
            {
                return "N/A";
            }

            if (sectorTiming.Lap.Valid)
            {
                return TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(sectorTiming.Duration, false);
            }

            if (!sectorTiming.Lap.Driver.Session.RetrieveAlsoInvalidLaps)
            {
                return "N/A";
            }

            if (sectorTiming.Lap.Driver.Session.SessionType == SessionType.Race || !sectorTiming.Lap.PitLap)
            {
                return TimeSpanFormatHelper.FormatTimeSpanOnlySeconds(sectorTiming.Duration, false);
            }

            return "N/A";
        }

        private string GetSector1()
        {
            return GetSectorTiming(LapInfo.Sector1);
        }

        private string GetSector2()
        {
            return GetSectorTiming(LapInfo.Sector2);
        }

        private string GetSector3()
        {
            return GetSectorTiming(LapInfo.Sector3);
        }

        private string GetLapTime()
        {
            if (!LapInfo.Valid && LapInfo.Driver.Session.SessionType != SessionType.Race && (LapInfo.Driver.InPits || LapInfo.PitLap))
            {
                return LapInfo.PitLap ? "Out Lap" : "Pit Lap";
            }

            if (!LapInfo.Valid && !LapInfo.Driver.Session.RetrieveAlsoInvalidLaps)
            {
                return "Lap Invalid";
            }

            return LapInfo.Completed ? TimeSpanFormatHelper.FormatTimeSpan(LapInfo.LapTime) : TimeSpanFormatHelper.FormatTimeSpan(LapInfo.CurrentlyValidProgressTime);
        }
    }
}