﻿namespace SecondMonitor.Timing.ReportCreation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataModel.Summary;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot.Drivers;
    using NLog;
    using SecondMonitor.Timing.SessionTiming.Drivers.ViewModel;
    using SecondMonitor.Timing.SessionTiming.ViewModel;

    public static class SessionTimingExtension
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static SessionSummary ToSessionSummary(this SessionTiming timing, bool overridePlayerAsFinished)
        {
            SessionSummary summary = new SessionSummary();
            FillSessionInfo(summary, timing);
            AddDrivers(summary, timing, overridePlayerAsFinished);
            return summary;
        }

        private static void FillSessionInfo(SessionSummary summary, SessionTiming timing)
        {
            summary.SessionType = timing.SessionType;
            summary.TrackInfo = timing.LastSet.SessionInfo.TrackInfo;
            summary.Simulator = timing.LastSet.Source;
            summary.SessionLength = TimeSpan.FromSeconds(timing.TotalSessionLength);
            summary.SessionLengthType = timing.LastSet.SessionInfo.SessionLengthType;
            summary.TotalNumberOfLaps = timing.LastSet.SessionInfo.TotalNumberOfLaps;
            summary.SessionRunDuration = timing.SessionTime;
            summary.WasGreen = timing.WasGreen;
            summary.IsMultiClass = timing.IsMultiClass;
            summary.FuelConsumptionInformation = timing.TimingApplicationViewModel.FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo;
            summary.SessionGuid = timing.GlobalKey;
        }

        private static void AddDrivers(SessionSummary summary, SessionTiming timing, bool overridePlayerAsFinished)
        {
            summary.Drivers.AddRange(timing.Drivers.Where(x => x.Value.IsActive).Select(d => ConvertToSummaryDriver(d.Value, timing.SessionType, timing.Player, overridePlayerAsFinished)));
            FillRaceGapsInfo(summary.Drivers, summary);
            AddPitStopInfo(summary, timing.Drivers.Values.FirstOrDefault(x => x.IsPlayer));
        }

        private static void AddPitStopInfo(SessionSummary summary, DriverTiming player)
        {
            if (player == null || summary.SessionType != SessionType.Race)
            {
                return;
            }

            summary.PitStops = player.PitStops.Select(CreatePitStopSummary).ToList();
        }

        private static PitStopSummary CreatePitStopSummary(PitStopInfo pitStopInfo)
        {
            return new PitStopSummary()
            {
                EntryLapNumber =  pitStopInfo.EntryLap?.LapNumber ?? 0,
                NewRearCompound = pitStopInfo.NewRearCompound,
                NewFrontCompound = pitStopInfo.NewFrontCompound,
                FuelTaken = pitStopInfo.FuelTaken,
                IsFrontTyresChanged = pitStopInfo.IsFrontTyresChanged,
                IsRearTyresChanged = pitStopInfo.IsRearTyresChanged,
                OverallDuration = pitStopInfo.PitStopDuration,
                StallDuration = pitStopInfo.PitStopStallDuration,
                WasDriverThrough = pitStopInfo.WasDriveThrough
            };
        }

        private static Driver ConvertToSummaryDriver(DriverTiming driverTiming, SessionType sessionType, DriverTiming playerTiming, bool overridePlayerAsFinished)
        {
            Driver driverSummary = new Driver()
                                       {
                                           DriverLongName = driverTiming.DriverLongName,
                                           CarName = driverTiming.CarName,
                                           Finished = sessionType != SessionType.Race || driverTiming.IsActive,
                                           FinishingPosition = driverTiming.Position,
                                           TopSpeed = driverTiming.TopSpeed,
                                           IsPlayer = driverTiming.DriverInfo.IsPlayer,
                                           FinishStatus = driverTiming.DriverInfo.FinishStatus,
                                           ClassName = driverTiming.CarClassName,
                                           ClassId = driverTiming.CarClassId,
                                           TotalDistance = driverTiming.TotalDistanceTraveled,
                                           GapToPlayerByTiming = driverTiming.GapToPlayerAbsolute,
                                           DriverId = driverTiming.DriverId,
                                       };
            ILapInfo[] lapsWithEndPosition = driverTiming.Laps.Where(x => x.LapEndPosition > 0).ToArray();

            driverSummary.AveragePosition = lapsWithEndPosition.Length > 0 ? lapsWithEndPosition.Average(x => x.LapEndPosition) : 0;
            if (driverSummary.IsPlayer && overridePlayerAsFinished)
            {
                driverSummary.FinishStatus = DriverFinishStatus.Finished;
            }
            int lapNumber = 1;
            bool allLaps = sessionType == SessionType.Race;
            Logger.Info($"Exporting Session Summary driver {driverSummary.DriverId}, Total Laps : {driverTiming.Laps.Count}, Average Position {driverSummary.AveragePosition:F2}");
            driverSummary.Laps.AddRange(driverTiming.Laps.Where(l => l.LapTelemetryInfo != null && l.Completed && (allLaps || l.Valid)).Select(l => ConvertToSummaryLap(driverSummary, l, lapNumber++, sessionType)));
            driverSummary.TotalLaps = driverSummary.Laps.Count;
            Logger.Info($"Laps Exported: {driverSummary.TotalLaps}");
            FillGapInfo(driverSummary, driverTiming, playerTiming, sessionType);
            return driverSummary;
        }

        private static void FillGapInfo(Driver driverToFill, DriverTiming driverToFillTiming, DriverTiming playerTiming, SessionType sessionType)
        {
            if (playerTiming == null)
            {
                return;
            }

            if (sessionType == SessionType.Race)
            {
                driverToFill.LapsDifferenceToPlayer = -(int) ((driverToFillTiming.TotalDistanceTraveled - playerTiming.TotalDistanceTraveled) / driverToFillTiming.Session.LastSet.SessionInfo.TrackInfo.LayoutLength.InMeters);
            }
            else if(playerTiming.BestLap != null && driverToFillTiming.BestLap != null)
            {
                driverToFill.GapToPlayerRelative = driverToFillTiming.BestLap.LapTime - playerTiming.BestLap.LapTime;
            }
        }

        private static void FillRaceGapsInfo(List<Driver> driversToFill, SessionSummary sessionSummary)
        {
            var player = driversToFill.FirstOrDefault(x => x.IsPlayer);
            if (player == null || sessionSummary.SessionType != SessionType.Race)
            {
                return;
            }

            driversToFill.Where(x => !x.IsPlayer && x.FinishStatus != DriverFinishStatus.Dnf).ForEach(x => FillRaceGapsInfo(x, player));

        }

        private static void FillRaceGapsInfo(Driver driverToFill, Driver player)
        {
            /*int lapsCount = Math.Min(driverToFill.Laps.Count, player.Laps.Count);
            double totalTimeDriver = driverToFill.Laps.Take(lapsCount).Sum(x => x.LapTime.TotalSeconds);
            double totalTimePlayer = player.Laps.Take(lapsCount).Sum(x => x.LapTime.TotalSeconds);
            driverToFill.GapToPlayerRelative = TimeSpan.FromSeconds(totalTimeDriver - totalTimePlayer);*/
            driverToFill.GapToPlayerRelative = driverToFill.GapToPlayerByTiming;
        }

        private static Lap ConvertToSummaryLap(Driver summaryDriver,  ILapInfo lapInfo, int lapNumber, SessionType sessionType)
        {
            Lap summaryLap = new Lap(summaryDriver, lapInfo.Valid)
                                 {
                                     IsPitLap = lapInfo.PitLap,
                                     LapNumber = lapNumber,
                                     LapTime = lapInfo.LapTime,
                                     Sector1 = lapInfo.Sector1?.Duration ?? TimeSpan.Zero,
                                     Sector2 = lapInfo.Sector2?.Duration ?? TimeSpan.Zero,
                                     Sector3 = lapInfo.Sector3?.Duration ?? TimeSpan.Zero,
                                     LapEndSnapshot = lapInfo.LapTelemetryInfo.LapEndSnapshot,
                                     LapStartSnapshot = lapInfo.LapTelemetryInfo.LapStarSnapshot,
                                     SessionType = sessionType,
            };
            return summaryLap;
        }
    }
}