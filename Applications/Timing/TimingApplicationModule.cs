﻿namespace SecondMonitor.Timing
{
    using Contracts.Session;
    using Contracts.TrackMap;
    using Contracts.TrackRecords;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using LapTimings;
    using LapTimings.ViewModel;
    using Layout;
    using Ninject;
    using Ninject.Extensions.NamedScope;
    using Ninject.Modules;
    using PitBoard.Controller;
    using PitBoard.DataProviders;
    using PitBoard.ViewModels;
    using SessionTiming;
    using Telemetry;
    using TrackRecords.Controller;
    using ViewModel;
    using ViewModels.CarStatus;
    using ViewModels.Layouts.Factory;
    using ViewModels.PitBoard.Controller;
    using ViewModels.Track.SituationOverview.Controller;

    public class TimingApplicationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITrackRecordsRepositoryFactory>().To<TrackRecordsRepositoryFactory>();
            Bind<ITrackRecordsController, ITrackRecordsProvider>().To<TrackRecordsController>().InSingletonScope();
            Bind<ISessionEventsController>().To<SessionEventsController>();
            Bind<DriverLapSectorsTrackerFactory>().ToSelf();
            Bind<PitBoardViewModel>().ToSelf();
            Bind<IPitBoardController, PitBoardController>().To<PitBoardController>().InSingletonScope();
            Bind<IAutonomousPitBoardDataProvider>().To<GapPitBoardProvider>();
            Bind<IAutonomousPitBoardDataProvider>().To<YellowAheadPitBoardProvider>();
            Bind<IAutonomousPitBoardDataProvider>().To<ClearToRejoinBoardProvider>();
            Bind<SessionTimingFactory>().ToSelf();
            Bind<TimingApplicationViewModel, IPaceProvider>().To<TimingApplicationViewModel>().InSingletonScope();
            Bind<DriverLapsWindowManager>().ToSelf().InSingletonScope();
            Bind<ISessionTelemetryControllerFactory>().To<SessionTelemetryControllerFactory>();
            Bind<IMapManagementController, MapManagementController>().To<MapManagementController>().InSingletonScope();
            Bind<SessionInfoViaTimingViewModelProvider>().To<SessionInfoViaTimingViewModelProvider>().InSingletonScope();
            Bind<ISessionInformationProvider>().ToMethod(c => c.Kernel.Get<SessionInfoViaTimingViewModelProvider>()).WhenInjectedExactlyInto<SituationOverviewController>();

            Bind<SettingsWindowController>().ToSelf().DefinesNamedScope("SettingsWindow");
            Bind<IDefaultLayoutFactory>().To<TimingApplicationDefaultLayoutFactory>().InNamedScope("SettingsWindow");
            Bind<ILayoutElementNamesProvider>().To<TimingApplicationElementNamesProvider>().InNamedScope("SettingsWindow");
            Bind<ISimulatorDataSetVisitor>().To<ForceSingleClassVisitor>();
        }
    }
}