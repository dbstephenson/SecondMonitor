﻿namespace SecondMonitor.Timing.PitBoard.DataProviders
{
    using System.Collections.Generic;
    using Controller;
    using DataModel.Snapshot;
    using SessionTiming.Drivers.ViewModel;

    public interface IAutonomousPitBoardDataProvider
    {
        void StartDataProvider(PitBoardController pitBoardController);

        void OnPlayerCompletedLap(SimulatorDataSet dataSet, IReadOnlyCollection<DriverTiming> driverTimingsModels);
        void Reset();
    }
}