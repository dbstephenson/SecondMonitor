﻿namespace SecondMonitor.Timing.PitBoard.ViewModels
{
    using SecondMonitor.ViewModels;

    public class PitBoardViewModel : AbstractViewModel
    {
        private bool _isPitBoardVisible;
        private IViewModel _pitBoard;

        public PitBoardViewModel()
        {
            IsPitBoardVisible = false;
        }

        public bool IsPitBoardVisible
        {
            get => _isPitBoardVisible;
            set => SetProperty(ref _isPitBoardVisible, value);
        }

        public IViewModel PitBoard
        {
            get => _pitBoard;
            set => SetProperty(ref _pitBoard, value);
        }
    }
}