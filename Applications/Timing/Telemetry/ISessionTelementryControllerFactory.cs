﻿namespace SecondMonitor.Timing.Telemetry
{
    using DataModel.Snapshot;
    using SecondMonitor.Telemetry.TelemetryApplication.Repository;
    using ViewModels.Settings;

    public class SessionTelemetryControllerFactory : ISessionTelemetryControllerFactory
    {
        private readonly ITelemetryRepositoryFactory _telemetryRepositoryFactory;
        private readonly ISettingsProvider _settingsProvider;

        public SessionTelemetryControllerFactory(ITelemetryRepositoryFactory telemetryRepositoryFactory, ISettingsProvider settingsProvider)
        {
            _telemetryRepositoryFactory = telemetryRepositoryFactory;
            _settingsProvider = settingsProvider;
        }

        public ISessionTelemetryController Create(SimulatorDataSet simulatorDataSet)
        {
            return new SessionTelemetryController(simulatorDataSet.SessionInfo.TrackInfo.TrackName, simulatorDataSet.SessionInfo.SessionType, _telemetryRepositoryFactory.Create(_settingsProvider));
        }
    }
}