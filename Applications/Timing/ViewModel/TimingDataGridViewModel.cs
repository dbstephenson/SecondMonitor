﻿namespace SecondMonitor.Timing.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using Contracts.Session;
    using Contracts.SimSettings;
    using DataModel.BasicProperties;
    using DataModel.DriversPresentation;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using NLog;
    using SessionTiming.Drivers.Presentation.ViewModel;
    using SessionTiming.Drivers.ViewModel;
    using SimdataManagement.DriverPresentation;
    using ViewModels;
    using ViewModels.Colors;
    using ViewModels.SessionEvents;
    using ViewModels.Settings;
    using ViewModels.Settings.Model;
    using ViewModels.Settings.ViewModel;

    public class TimingDataGridViewModel : AbstractViewModel, ISessionInformationProvider
    {
        public const string ViewModelLayoutName = "Timing Data Grid";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly DriverPresentationsManager _driverPresentationsManager;
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;
        private readonly IClassColorProvider _classColorProvider;
        public readonly SessionInfoViaTimingViewModelProvider _sessionInfoViaTimingViewModelProvider;
        private readonly object _lockObject = new object();
        private readonly Dictionary<string, DriverTiming> _driverNameTimingMap;
        private int _loadIndex;
        private readonly Stopwatch _refreshGapWatch;
        private int _currentRefreshCarIndex;
        private SessionOptionsViewModel _currentSessionOptionsViewModel;
        private ICommand _openSelectedDriversLapsCommand;
        private DriverTimingViewModel _selectedDriverTimingViewModel;

        public TimingDataGridViewModel(DriverPresentationsManager driverPresentationsManager, ISettingsProvider settingsProvider, IClassColorProvider classColorProvider, SessionInfoViaTimingViewModelProvider sessionInfoViaTimingViewModelProvider, ISessionEventProvider sessionEventProvider )
        {
            _refreshGapWatch = Stopwatch.StartNew();
            _loadIndex = 0;
            _driverNameTimingMap = new Dictionary<string, DriverTiming>();
            _driverPresentationsManager = driverPresentationsManager;
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            _classColorProvider = classColorProvider;
            _sessionInfoViaTimingViewModelProvider = sessionInfoViaTimingViewModelProvider;
            _sessionInfoViaTimingViewModelProvider.RegisterRelay(this);
            DriversViewModels = new ObservableCollection<DriverTimingViewModel>();
            _driverPresentationsManager.DriverCustomColorChanged += DriverPresentationsManagerOnDriverCustomColorEnabledChanged;
            CurrentSessionOptionsViewModel = _displaySettingsViewModel.PracticeSessionDisplayOptionsView;
            sessionEventProvider.SessionTypeChange+= SessionEventProviderOnSessionTypeChange;
        }

        public SessionOptionsViewModel CurrentSessionOptionsViewModel
        {
            get => _currentSessionOptionsViewModel;
            set => SetProperty(ref _currentSessionOptionsViewModel, value);
        }

        public ICommand OpenSelectedDriversLapsCommand
        {
            get => _openSelectedDriversLapsCommand;
            set => SetProperty(ref _openSelectedDriversLapsCommand, value);
        }

        public ObservableCollection<DriverTimingViewModel> DriversViewModels { get; }

        public DriverTimingViewModel PlayerViewModel { get; set; }

        public DriverTimingViewModel SelectedDriverTimingViewModel
        {
            get => _selectedDriverTimingViewModel;
            set => SetProperty(ref _selectedDriverTimingViewModel, value);
        }

        public bool IsScrollToPlayerEnabled => _displaySettingsViewModel.ScrollToPlayer;


        public void UpdateProperties(SimulatorDataSet dataSet)
        {

            if (_loadIndex > 0 || dataSet == null)
            {
                return;
            }

            int driversUpdatedPerTick = _displaySettingsViewModel.DriversUpdatedPerTick;
            lock (_lockObject)
            {
                List<DriverTiming> orderedTimings = (CurrentSessionOptionsViewModel.OrderingMode == DisplayModeEnum.Absolute || dataSet.SessionInfo.SessionPhase == SessionPhase.Countdown ? _driverNameTimingMap.Values.OrderBy(x => x.Position) : _driverNameTimingMap.Values.OrderBy(x => x.DistanceToPlayer)).ToList();
                for (int i = _currentRefreshCarIndex; i < orderedTimings.Count && i - _currentRefreshCarIndex < driversUpdatedPerTick; i++)
                {
                    RebindViewModel(DriversViewModels[i], orderedTimings[i]);
                    if (DriversViewModels[i].IsPlayer)
                    {
                        PlayerViewModel = DriversViewModels[i];
                    }
                    DriversViewModels[i].RefreshProperties(dataSet);
                }

                _currentRefreshCarIndex += driversUpdatedPerTick;

                if (_currentRefreshCarIndex >= dataSet.DriversInfo.Length)
                {
                    _currentRefreshCarIndex = 0;
                }

                if (_refreshGapWatch.ElapsedMilliseconds < 500)
                {
                    return;
                }

                UpdateGapsSize(dataSet);
                _refreshGapWatch.Restart();
            }
        }

        private void RebindViewModel(DriverTimingViewModel driverTimingViewModel, DriverTiming driverTiming)
        {
            if (driverTimingViewModel.DriverTiming == driverTiming)
            {
                return;
            }

            if (driverTimingViewModel.DriverTiming.CarClassId != driverTiming.CarClassId)
            {
                driverTimingViewModel.ClassIndicationBrush = _classColorProvider.GetColorForClass(driverTiming.CarClassId);
            }

            driverTimingViewModel.OutLineColor = GetDriverOutline(driverTiming.DriverLongName);
            driverTimingViewModel.DriverTiming = driverTiming;
        }

        private ColorDto GetDriverOutline(string driverLongName)
        {
            _driverPresentationsManager.TryGetDriverPresentation(driverLongName, out DriverPresentationDto driverPresentationDto);
            return driverPresentationDto?.CustomOutLineEnabled == true ? driverPresentationDto.OutLineColor  : null;
        }

        private void UpdateGapsSize(SimulatorDataSet dataSet)
        {
            if (dataSet?.SessionInfo == null || dataSet.SessionInfo.SessionType != SessionType.Race || CurrentSessionOptionsViewModel.OrderingMode != DisplayModeEnum.Relative)
            {

                return;
            }

            bool isVisualizationEnabled = _displaySettingsViewModel.IsGapVisualizationEnabled;
            double minimalGap = _displaySettingsViewModel.MinimalGapForVisualization;
            double heightPerSecond = _displaySettingsViewModel.GapHeightForOneSecond;
            double maximumGap = _displaySettingsViewModel.MaximumGapHeight;

            DriverTimingViewModel previousViewModel = null;
            foreach (DriverTimingViewModel driverTimingViewModel in DriversViewModels)
            {
                if (previousViewModel == null)
                {
                    previousViewModel = driverTimingViewModel;
                    continue;
                }

                /*if ((previousViewModel.IsLapping && driverTimingViewModel.IsLapping) || (driverTimingViewModel.IsLapped &&  driverTimingViewModel.IsLapped))
                {
                    previousViewModel.GapHeight = 0;
                    previousViewModel = driverTimingViewModel;
                    continue;
                }*/

                double gapInSeconds = Math.Abs(driverTimingViewModel.GapToPlayer.TotalSeconds - previousViewModel.GapToPlayer.TotalSeconds);
                double gapInSecondsWithMinimal = gapInSeconds - minimalGap;
                if (!isVisualizationEnabled || gapInSecondsWithMinimal < 0 )
                {
                    previousViewModel.GapHeight = 0;
                    previousViewModel = driverTimingViewModel;
                    continue;
                }

                previousViewModel.GapToNextDriver = gapInSeconds;
                previousViewModel.GapHeight = Math.Round(Math.Min(gapInSecondsWithMinimal * heightPerSecond, maximumGap));
                previousViewModel = driverTimingViewModel;

            }
        }

        public void RemoveDriver(DriverTiming driver)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => RemoveDriver(driver));
                return;
            }

            lock (_lockObject)
            {
                DriverTimingViewModel toRemove = DriversViewModels.FirstOrDefault(x => x.DriverTiming == driver);
                if (toRemove == null)
                {
                    return;
                }

                _driverNameTimingMap.Remove(driver.DriverId);
                DriversViewModels.Remove(toRemove);
            }
        }

        public void AddDriver(DriverTiming driver)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => AddDriver(driver));
                return;
            }

            DriverTimingViewModel newViewModel = new DriverTimingViewModel(driver) {ClassIndicationBrush = GetClassColor(driver.DriverInfo) };
            lock (_lockObject)
            {
                //If possible, rebind - do not create new
                if (_driverNameTimingMap.ContainsKey(driver.DriverId))
                {
                    _driverNameTimingMap[driver.DriverId] = driver;
                    RebindViewModel(DriversViewModels.First(x => x.DriverId == driver.DriverId), driver);
                    return;
                }
                _driverNameTimingMap[driver.DriverId] = driver;
                newViewModel.OutLineColor = GetDriverOutline(driver.DriverLongName);
                DriversViewModels.Add(newViewModel);
            }
        }

        public void MatchDriversList(List<DriverTiming> drivers)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => MatchDriversList(drivers));
                return;
            }

            _currentRefreshCarIndex = 0;
            DriversViewModels.ForEach(x => x.GapHeight = 0);

            lock (_lockObject)
            {
                _driverNameTimingMap.Clear();
                DriversViewModels.Clear();
                drivers.ForEach(AddDriver);
            }
        }

        private void AddDrivers(List<DriverTiming> drivers)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => AddDrivers(drivers));
                return;
            }

            try
            {
                _loadIndex++;
                Logger.Info("Add Drivers Called");
                foreach (DriverTiming driverTiming in drivers)
                {
                    Logger.Info($"Driver {driverTiming.DriverId}, of class {driverTiming.CarClassId} ");
                }
                List<DriverTimingViewModel> newViewModels = drivers.Select(x => new DriverTimingViewModel(x)
                {
                    ClassIndicationBrush = GetClassColor(x.DriverInfo),
                    OutLineColor = GetDriverOutline(x.DriverLongName)
                }).ToList();

                foreach (DriverTimingViewModel driverTimingViewModel in newViewModels)
                {
                    lock (_lockObject)
                    {
                        if (_driverNameTimingMap.ContainsKey(driverTimingViewModel.DriverId))
                        {
                            continue;
                        }
                        _driverNameTimingMap[driverTimingViewModel.DriverId] = driverTimingViewModel.DriverTiming;
                        DriversViewModels.Add(driverTimingViewModel);
                    }
                }

                _loadIndex--;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            Logger.Info("Add Drivers Completed");
        }

        public bool IsDriverOnValidLap(IDriverInfo driver)
        {
            if (driver?.DriverSessionId == null)
            {
                return false;
            }

            lock (_lockObject)
            {
                if (_driverNameTimingMap.TryGetValue(driver.DriverSessionId, out DriverTiming timing))
                {
                    return timing.CurrentLap?.Valid ?? false;
                }
            }

            return false;
        }

        public bool IsDriverLastSectorGreen(IDriverInfo driver, int sectorNumber)
        {
            if (driver?.DriverSessionId == null)
            {
                return false;
            }

            DriverTiming timing;
            lock (_lockObject)
            {
                if (!_driverNameTimingMap.TryGetValue(driver.DriverSessionId, out timing))
                {
                    return false;
                }
            }

            switch (sectorNumber)
            {
                case 1:
                    return timing.IsLastSector1PersonalBest;
                case 2:
                    return timing.IsLastSector2PersonalBest;
                case 3:
                    return timing.IsLastSector3PersonalBest;
                default:
                    return false;
            }
        }

        public bool IsDriverLastSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            if (driver?.DriverSessionId == null)
            {
                return false;
            }
            DriverTiming timing;
            lock (_lockObject)
            {
                if (!_driverNameTimingMap.TryGetValue(driver.DriverSessionId, out timing))
                {
                    return false;
                }
            }

            switch (sectorNumber)
            {
                case 1:
                    return timing.IsLastSector1SessionBest;
                case 2:
                    return timing.IsLastSector2SessionBest;
                case 3:
                    return timing.IsLastSector3SessionBest;
                default:
                    return false;
            }
        }

        public bool IsDriverLastClassSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            if (driver?.DriverSessionId == null)
            {
                return false;
            }
            DriverTiming timing;
            lock (_lockObject)
            {
                if (!_driverNameTimingMap.TryGetValue(driver.DriverSessionId, out timing))
                {
                    return false;
                }
            }

            switch (sectorNumber)
            {
                case 1:
                    return timing.IsLastSector1ClassSessionBest;
                case 2:
                    return timing.IsLastSector2ClassSessionBest;
                case 3:
                    return timing.IsLastSector3ClassSessionBest;
                default:
                    return false;
            }
        }

        private void SetSessionOptionOfCurrentSession(SimulatorDataSet dataSet)
        {
            if (dataSet == null)
            {
                return;
            }

            switch (dataSet.SessionInfo.SessionType)
            {
                case SessionType.Practice:
                case SessionType.WarmUp:
                    CurrentSessionOptionsViewModel = _displaySettingsViewModel.PracticeSessionDisplayOptionsView;
                    break;
                case SessionType.Qualification:
                    CurrentSessionOptionsViewModel = _displaySettingsViewModel.QualificationSessionDisplayOptionsView;
                    break;
                case SessionType.Race:
                    CurrentSessionOptionsViewModel = _displaySettingsViewModel.RaceSessionDisplayOptionsView;
                    break;
                default:
                    CurrentSessionOptionsViewModel = _displaySettingsViewModel.PracticeSessionDisplayOptionsView;
                    break;
            }
        }

        public ColorDto GetClassColor(IDriverInfo driverInfo)
        {
            return _classColorProvider.GetColorForClass(driverInfo.CarClassId);
        }

        private void DriverPresentationsManagerOnDriverCustomColorEnabledChanged(object sender, DriverCustomColorEnabledArgs e)
        {
            ColorDto colorToSet = e.IsCustomOutlineEnabled ? e.DriverColor : null;
            DriversViewModels.Where(x => x.DriverShortName == e.DriverLongName).ForEach(x => x.OutLineColor = colorToSet);
        }

        private void SessionEventProviderOnSessionTypeChange(object sender, DataSetArgs e)
        {
            SetSessionOptionOfCurrentSession(e.DataSet);
        }
    }
}