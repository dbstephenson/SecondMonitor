﻿namespace SecondMonitor.Timing.ViewModel
{
    public enum TimingDataViewModelResetModeEnum
    {
        NoReset,

        Manual,

        Automatic
    }
}