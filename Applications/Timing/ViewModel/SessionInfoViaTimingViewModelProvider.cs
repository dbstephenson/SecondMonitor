﻿namespace SecondMonitor.Timing.ViewModel
{
    using Contracts.Session;
    using DataModel.Snapshot.Drivers;

    public class SessionInfoViaTimingViewModelProvider : ISessionInformationProvider
    {
        private TimingDataGridViewModel _timingDataGridViewModel;

        public SessionInfoViaTimingViewModelProvider()
        {

        }

        public void RegisterRelay(TimingDataGridViewModel timingDataGridViewModel)
        {
            _timingDataGridViewModel = timingDataGridViewModel;
        }

        public bool IsDriverOnValidLap(IDriverInfo driver)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverOnValidLap(driver);
        }

        public bool IsDriverLastSectorGreen(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastSectorGreen(driver, sectorNumber);
        }

        public bool IsDriverLastSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastSectorPurple(driver, sectorNumber);
        }

        public bool IsDriverLastClassSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastClassSectorPurple(driver, sectorNumber);
        }
    }
}