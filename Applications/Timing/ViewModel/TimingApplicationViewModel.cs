﻿namespace SecondMonitor.Timing.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using Commands;
    using DataModel.BasicProperties;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using LapTimings.ViewModel;
    using NLog;
    using PitBoard.Controller;
    using PluginManager.GameConnector;
    using Rating.Application.Rating.ViewModels;
    using ReportCreation;
    using SessionTiming;
    using SessionTiming.Drivers;
    using SessionTiming.Drivers.ViewModel;
    using SessionTiming.ViewModel;
    using SimdataManagement.RaceSuggestion.ViewModels;
    using TrackRecords.Controller;
    using ViewModels;
    using ViewModels.CarStatus;
    using ViewModels.CarStatus.FuelStatus;
    using ViewModels.Controllers;
    using ViewModels.Factory;
    using ViewModels.Layouts;
    using ViewModels.Layouts.Factory;
    using ViewModels.SessionEvents;
    using ViewModels.Settings;
    using ViewModels.Settings.Model;
    using ViewModels.Settings.ViewModel;
    using ViewModels.Track.SituationOverview;
    using ViewModels.Track.SituationOverview.Controller;
    using ViewModels.TrackInfo;
    using ViewModels.TrackRecords;

    public class TimingApplicationViewModel : AbstractViewModel, IPaceProvider
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly DriverLapsWindowManager _driverLapsWindowManager;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly SessionTimingFactory _sessionTimingFactory;
        private readonly IViewModelFactory _viewModelFactory;
        private readonly ILayoutFactory _layoutFactory;

        private ICommand _resetCommand;

        private TimingDataViewModelResetModeEnum _shouldReset = TimingDataViewModelResetModeEnum.NoReset;

        private SessionTiming _sessionTiming;
        private SessionType _sessionType = SessionType.Na;
        private SimulatorDataSet _lastDataSet;

        private bool _isNamesNotUnique;
        private string _notUniqueNamesMessage;
        private Stopwatch _notUniqueCheckWatch;

        private Task _refreshBasicInfoTask;
        private Task _refreshTimingCircleTask;
        private Task _refreshTimingGridTask;

        private string _connectedSource;
        private DisplaySettingsViewModel _displaySettingsViewModel;
        private IRatingApplicationViewModel _ratingApplicationViewModel;
        private readonly SituationOverviewController _situationOverviewController;
        private readonly CarAndFuelStatusViewModel _carAndFuelStatusViewModel;
        private readonly LapDeltaViewModel _lapDeltaViewModel;
        private readonly SessionInfoViewModel _sessionInfoViewModel;
        private readonly TrackInfoViewModel _trackInfoViewModel;
        private CarStatusViewModel _carStatusViewModel;
        private IViewModel _mainViewModel;
        private readonly CachedViewModelsFactory _viewModelsCache;
        private SuggestionsViewModel _suggestionsViewModel;


        public TimingApplicationViewModel(ISettingsProvider settingsProvider, ITrackRecordsController trackRecordsController, DriverLapsWindowManager driverLapsWindowManager,
            ISessionEventProvider sessionEventProvider, PitBoardController pitBoardController, SessionTimingFactory sessionTimingFactory, SituationOverviewController situationOverviewController, IViewModelFactory viewModelFactory, ILayoutFactory layoutFactory)
        {
            _viewModelsCache = new CachedViewModelsFactory();
            SidebarViewModel = new SidebarViewModel();
            PitBoardController = pitBoardController;
            _situationOverviewController = situationOverviewController;
            _situationOverviewController.PropertyChanged += SituationOverviewControllerOnPropertyChanged;
            TimingDataGridViewModel = viewModelFactory.Create<TimingDataGridViewModel>();
            _sessionInfoViewModel = new SessionInfoViewModel();
            _trackInfoViewModel = new TrackInfoViewModel();
            _driverLapsWindowManager = driverLapsWindowManager;
            _sessionEventProvider = sessionEventProvider;
            _sessionEventProvider.PlayerFinishStateChanged += SessionEventProviderOnPlayerFinishStateChanged;
            _sessionTimingFactory = sessionTimingFactory;
            _viewModelFactory = viewModelFactory;
            _layoutFactory = layoutFactory;
            DisplaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            CurrentSessionOptionsView = DisplaySettingsViewModel.PracticeSessionDisplayOptionsView;
            TrackRecordsViewModel = trackRecordsController.TrackRecordsViewModel;
            _lapDeltaViewModel = new LapDeltaViewModel()
            {
                DisplaySettingsViewModel = DisplaySettingsViewModel
            };

            _carAndFuelStatusViewModel = new CarAndFuelStatusViewModel()
            {
                DisplaySettingsViewModel = DisplaySettingsViewModel,
            };
        }

        public event EventHandler<SessionSummaryEventArgs> SessionCompleted;
        public event EventHandler<SessionSummaryEventArgs> PlayerFinished;

        public TimeSpan? PlayersPace => SessionTiming?.Player?.Pace;
        public TimeSpan? LeadersPace => SessionTiming?.Leader?.Pace;

        public PitBoardController PitBoardController { get; }

        public IViewModel MainViewModel
        {
            get => _mainViewModel;
            set => SetProperty(ref _mainViewModel, value);
        }

        public SuggestionsViewModel SuggestionsViewModel
        {
            get => _suggestionsViewModel;
            set => SetProperty(ref _suggestionsViewModel, value);
        }

        public SidebarViewModel SidebarViewModel { get; }


        public DisplaySettingsViewModel DisplaySettingsViewModel
        {
            get => _displaySettingsViewModel;
            private set
            {
                _displaySettingsViewModel = value;
                NotifyPropertyChanged();
                DisplaySettingsChanged();
            }
        }

        private SessionOptionsViewModel _currentSessionOptionsView;
        public SessionOptionsViewModel CurrentSessionOptionsView
        {
            get => _currentSessionOptionsView;
            set
            {
                SetProperty(ref _currentSessionOptionsView, value);
                ChangeOrderingMode();
                ChangeTimeDisplayMode();
            }
        }

        public FuelOverviewViewModel FuelOverviewViewModel
        {
            get => _carAndFuelStatusViewModel.FuelOverviewViewModel;
            set => _carAndFuelStatusViewModel.FuelOverviewViewModel = value;
        }
        public bool IsNamesNotUnique
        {
            get => _isNamesNotUnique;
            private set => SetProperty(ref _isNamesNotUnique, value);
        }

        public string NotUniqueNamesMessage
        {
            get => _notUniqueNamesMessage;
            private set => SetProperty(ref _notUniqueNamesMessage, value);
        }

        public ITrackRecordsViewModel TrackRecordsViewModel { get; }

        public TimingDataGridViewModel TimingDataGridViewModel { get; }

        public int SessionCompletedPercentage => _sessionTiming?.SessionCompletedPerMiles ?? 50;

        public ICommand ResetCommand => _resetCommand ?? (_resetCommand = new NoArgumentCommand(ScheduleReset));

        public string SessionTime => _sessionTiming?.SessionTime.FormatToDefault() ?? string.Empty;

        public string ConnectedSource
        {
            get => _connectedSource;
            set => SetProperty(ref _connectedSource, value);
        }

        public string SystemTime => DateTime.Now.ToString("HH:mm");

        public string Title
        {
            get
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                string version = AssemblyName.GetAssemblyName(assembly.Location).Version.ToString();
                return "Second Monitor (Timing)(v" + version + " )";
            }
        }

        public SessionTiming SessionTiming
        {
            get => _sessionTiming;
            private set => SetProperty(ref _sessionTiming, value);
        }

        public IRatingApplicationViewModel RatingApplicationViewModel
        {
            get => _ratingApplicationViewModel;
            set => SetProperty(ref _ratingApplicationViewModel, value);
        }

        private bool TerminatePeriodicTasks { get; set; }

        public Dictionary<string, TimeSpan> GetPaceForDriversMap()
        {
            if (SessionTiming == null)
            {
                return new Dictionary<string, TimeSpan>();
            }

            return SessionTiming.Drivers.ToDictionary(x => x.Value.DriverId, x => x.Value.Pace);
        }


        public void TerminatePeriodicTask(List<Exception> exceptions)
        {
            TerminatePeriodicTasks = true;
            if (_refreshBasicInfoTask.IsFaulted && _refreshBasicInfoTask.Exception != null)
            {
                exceptions.AddRange(_refreshBasicInfoTask.Exception.InnerExceptions);
            }

            if (_refreshTimingCircleTask.IsFaulted && _refreshTimingCircleTask.Exception != null)
            {
                exceptions.AddRange(_refreshTimingCircleTask.Exception.InnerExceptions);
            }

            if (_refreshTimingGridTask.IsFaulted && _refreshTimingGridTask.Exception != null)
            {
                exceptions.AddRange(_refreshTimingGridTask.Exception.InnerExceptions);
            }
        }

        public void ApplyDateSet(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }

            SidebarViewModel.IsOpenCarSettingsCommandEnable = !string.IsNullOrWhiteSpace(data?.PlayerInfo?.CarName);
            ConnectedSource = data.Source;
            if (_sessionTiming == null || data.SessionInfo.SessionType == SessionType.Na || data.SessionInfo.SessionPhase == SessionPhase.Unavailable)
            {
                return;
            }

            _lastDataSet = data;

            if (_sessionType != data.SessionInfo.SessionType)
            {
                _shouldReset = TimingDataViewModelResetModeEnum.Automatic;
                _sessionType = _sessionTiming.SessionType;
            }

            // Reset state was detected (either reset button was pressed or timing detected a session change)
            if (_shouldReset != TimingDataViewModelResetModeEnum.NoReset)
            {
                CheckAndNotifySessionCompleted();
                CreateTiming(data);
                _shouldReset = TimingDataViewModelResetModeEnum.NoReset;
            }

            try
            {
                CheckIdsUniques(data);
                _sessionTiming?.UpdateTiming(data);
                _carStatusViewModel?.PedalsAndGearViewModel?.ApplyDateSet(data);
                _carStatusViewModel?.UpdateTyresSlipInformation(data);
            }
            catch (DriverNotFoundException)
            {
                _shouldReset = TimingDataViewModelResetModeEnum.Automatic;
            }
        }

        public void DisplayMessage(MessageArgs e)
        {
            if (e.IsDecision)
            {
                if (MessageBox.Show(
                        e.Message,
                        "Message from connector.",
                        MessageBoxButton.YesNo,
                        MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    e.Action();
                }
            }
            else
            {
                MessageBox.Show(e.Message, "Message from connector.", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        public void Initialize()
        {
            _carStatusViewModel = _viewModelFactory.Create<CarStatusViewModel>();
            _carAndFuelStatusViewModel.CarStatusViewModel = _carStatusViewModel;
            ConnectedSource = "Not Connected";

            if (Application.Current?.Dispatcher != null && Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(ScheduleRefreshActions);
            }
            else
            {
                ScheduleRefreshActions();
            }

            OnDisplaySettingsChange(this, null);
            _shouldReset = TimingDataViewModelResetModeEnum.NoReset;
            new AutoUpdateController().CheckForUpdate();

            _viewModelsCache.RegisterViewModel(CarWheelsViewModel.ViewModelLayoutName, _carStatusViewModel.PlayersWheelsViewModel);
            _viewModelsCache.RegisterViewModel(CarSystemsViewModel.ViewModelLayoutName, _carStatusViewModel.CarSystemsViewModel);
            _viewModelsCache.RegisterViewModel(DashboardViewModel.ViewModelLayoutName, _carStatusViewModel.DashboardViewModel);
            _viewModelsCache.RegisterViewModel(PedalsAndGearViewModel.ViewModelLayoutName, _carStatusViewModel.PedalsAndGearViewModel);
            _viewModelsCache.RegisterViewModel(TrackInfoViewModel.ViewModelLayoutName, _trackInfoViewModel);
            _viewModelsCache.RegisterViewModel(LapDeltaViewModel.ViewModelLayoutName, _lapDeltaViewModel);
            _viewModelsCache.RegisterViewModel(FuelOverviewViewModel.ViewModelLayoutName, FuelOverviewViewModel);
            _viewModelsCache.RegisterViewModel(ViewModels.TrackRecords.TrackRecordsViewModel.ViewModelLayoutName, TrackRecordsViewModel);
            _viewModelsCache.RegisterViewModel(SessionInfoViewModel.ViewModelLayoutName, _sessionInfoViewModel);
            _viewModelsCache.RegisterViewModel(DefaultSituationOverviewViewModel.ViewModelLayoutName, new AutoUpdateViewModelWrapper(_situationOverviewController, () => _situationOverviewController.SituationOverviewViewModel));
            _viewModelsCache.RegisterViewModel(SidebarViewModel.ViewModelLayoutName, SidebarViewModel);
            _viewModelsCache.RegisterViewModel(Rating.Application.Rating.ViewModels.RatingApplicationViewModel.ViewModelLayoutName, new AutoUpdateViewModelWrapper(this, () => RatingApplicationViewModel));
            _viewModelsCache.RegisterViewModel(TimingDataGridViewModel.ViewModelLayoutName, TimingDataGridViewModel);

            CreateMainViewModel();
        }

        private void ScheduleRefreshActions()
        {
            _refreshBasicInfoTask = SchedulePeriodicAction(() => RefreshBasicInfo(_lastDataSet), () => 100, this, false);
            _refreshTimingCircleTask = SchedulePeriodicAction(() => RefreshTimingCircle(_lastDataSet), () => 100, this, false);
            _refreshTimingGridTask = SchedulePeriodicAction(() => RefreshTimingGrid(_lastDataSet), () => DisplaySettingsViewModel.RefreshRate, this, false);
        }

        private void RefreshTimingGrid(SimulatorDataSet lastDataSet)
        {
            if (lastDataSet == null)
            {
                return;
            }
            TimingDataGridViewModel.UpdateProperties(lastDataSet);
        }

        private void PaceLapsChanged()
        {
            if (_sessionTiming != null)
            {
                _sessionTiming.PaceLaps = DisplaySettingsViewModel.PaceLaps;
            }

        }

        private void CheckIdsUniques(SimulatorDataSet dataSet)
        {
            if (_notUniqueCheckWatch == null || _notUniqueCheckWatch.ElapsedMilliseconds < 10000)
            {
                return;
            }

            List<IGrouping<string, string>> namesGrouping = dataSet.DriversInfo.Select(x => x.DriverSessionId).GroupBy(x => x).ToList();
            List<string> uniqueNames = namesGrouping.Where(x => x.Count() == 1).SelectMany(x => x).ToList();
            List<string> notUniqueNames = namesGrouping.Where(x => x.Count() > 1).Select(x => x.Key).ToList();


            if (notUniqueNames.Count == 0)
            {
                IsNamesNotUnique = false;
                return;
            }

            IsNamesNotUnique = true;
            NotUniqueNamesMessage = $"Not All Driver Ids are unique: Number of unique drivers - {uniqueNames.Count}, Not unique names - {string.Join(", ", notUniqueNames)} ";
            _notUniqueCheckWatch.Restart();
        }

        private void ScheduleReset()
        {
            _shouldReset = TimingDataViewModelResetModeEnum.Manual;
        }

        private void ChangeOrderingMode()
        {
            if (_sessionTiming == null)
            {
                return;
            }

            var mode = GetOrderTypeFromSettings();
            _sessionTiming.DisplayGapToPlayerRelative = mode != DisplayModeEnum.Absolute;

        }

        private void ChangeTimeDisplayMode()
        {
            if (_sessionTiming == null || Application.Current?.Dispatcher == null)
            {
                return;
            }

            var mode = GetTimeDisplayTypeFromSettings();
            _sessionTiming.DisplayBindTimeRelative = mode == DisplayModeEnum.Relative;
            _sessionTiming.DisplayGapToPlayerRelative = mode == DisplayModeEnum.Relative;
        }

        private DisplayModeEnum GetOrderTypeFromSettings()
        {
            return CurrentSessionOptionsView.OrderingMode;
        }

        private DisplayModeEnum GetTimeDisplayTypeFromSettings()
        {
            return CurrentSessionOptionsView.TimesDisplayMode;
        }

        private SessionOptionsViewModel GetSessionOptionOfCurrentSession(SimulatorDataSet dataSet)
        {
            if (dataSet == null)
            {
                return new SessionOptionsViewModel();
            }

            switch (dataSet.SessionInfo.SessionType)
            {
                case SessionType.Practice:
                case SessionType.WarmUp:
                    return DisplaySettingsViewModel.PracticeSessionDisplayOptionsView;
                case SessionType.Qualification:
                    return DisplaySettingsViewModel.QualificationSessionDisplayOptionsView;
                case SessionType.Race:
                    return DisplaySettingsViewModel.RaceSessionDisplayOptionsView;
                default:
                    return DisplaySettingsViewModel.PracticeSessionDisplayOptionsView;
            }
        }

        private void RefreshTimingCircle(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }
            _situationOverviewController.ApplyDateSet(data);
        }

        private void RefreshBasicInfo(SimulatorDataSet data)
        {
            if (data == null)
            {
                return;
            }

            NotifyPropertyChanged(nameof(SessionTime));
            NotifyPropertyChanged(nameof(SystemTime));
            NotifyPropertyChanged(nameof(SessionCompletedPercentage));
            _carStatusViewModel.ApplyDateSet(data);
            _trackInfoViewModel.ApplyDateSet(data);
            _sessionInfoViewModel.ApplyDateSet(data);
        }

        private void SessionTimingDriverRemoved(object sender, DriverListModificationEventArgs e)
        {
            if (TerminatePeriodicTasks)
            {
                return;
            }
            TimingDataGridViewModel.RemoveDriver(e.Data);

            Application.Current.Dispatcher?.Invoke(() =>
            {
                _situationOverviewController.RemoveDriver(e.Data.DriverInfo);
            });

        }

        private void SessionTimingDriverAdded(object sender, DriverListModificationEventArgs e)
        {
            if (e.Data.IsPlayer)
            {
                _lapDeltaViewModel.Driver = e.Data;
            }
            TimingDataGridViewModel.AddDriver(e.Data);
            _situationOverviewController.AddDriver(e.Data.DriverInfo);
            _driverLapsWindowManager.Rebind(TimingDataGridViewModel.DriversViewModels.FirstOrDefault(x => x.DriverShortName == e.Data.DriverShortName));
        }

        private void CreateTiming(SimulatorDataSet data)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => CreateTiming(data));
                return;
            }

            if (SessionTiming != null)
            {
                SessionTiming.DriverAdded -= SessionTimingDriverAdded;
                SessionTiming.DriverRemoved -= SessionTimingDriverRemoved;
                SessionTiming.LapCompleted -= SessionTimingOnLapCompleted;
            }

            bool invalidateLap = _shouldReset == TimingDataViewModelResetModeEnum.Manual ||
                                data.SessionInfo.SessionType != SessionType.Race;
            _lastDataSet = data;
            SessionTiming = _sessionTimingFactory.Create(data, this, invalidateLap);
            _lapDeltaViewModel.Driver = SessionTiming.Player;
            _sessionInfoViewModel.SessionTiming = _sessionTiming;
            SessionTiming.DriverAdded += SessionTimingDriverAdded;
            SessionTiming.DriverRemoved += SessionTimingDriverRemoved;
            SessionTiming.LapCompleted += SessionTimingOnLapCompleted;
            SessionTiming.PaceLaps = DisplaySettingsViewModel.PaceLaps;

            _carStatusViewModel.Reset();
            _trackInfoViewModel.Reset();
            _situationOverviewController.Reset();
            PitBoardController.Reset();


            InitializeGui(data);
            ChangeTimeDisplayMode();
            ChangeOrderingMode();
            ConnectedSource = data.Source;
            _notUniqueCheckWatch = Stopwatch.StartNew();
            NotifyPropertyChanged(nameof(ConnectedSource));
        }

        private void SessionTimingOnLapCompleted(object sender, LapEventArgs e)
        {
            try
            {
                if (e.Lap.Driver.IsPlayer)
                {
                    PitBoardController.OnPlayerCompletedLap(_sessionEventProvider.LastDataSet, _sessionTiming.Drivers.Values);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void StartNewSession(SimulatorDataSet dataSet)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => StartNewSession(dataSet));
                return;
            }
            ConnectedSource = dataSet.Source;
            CheckAndNotifySessionCompleted();
            if (dataSet.SessionInfo.SessionType == SessionType.Na)
            {
                return;
            }
            _sessionInfoViewModel.Reset();
            UpdateCurrentSessionOption(dataSet);
            CreateTiming(dataSet);
        }

        private void UpdateCurrentSessionOption(SimulatorDataSet data)
        {
            CurrentSessionOptionsView = GetSessionOptionOfCurrentSession(data);
        }

        private void InitializeGui(SimulatorDataSet data)
        {
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => InitializeGui(data));
                return;
            }

            if (data.SessionInfo.SessionType != SessionType.Na)
            {
                TimingDataGridViewModel.MatchDriversList(_sessionTiming.Drivers.Values.ToList());
                _driverLapsWindowManager.RebindAll(TimingDataGridViewModel.DriversViewModels);
            }

            _situationOverviewController.ApplyDateSet(data);
        }

        private void OnDisplaySettingsChange(object sender, PropertyChangedEventArgs args)
        {
            ApplyDisplaySettings(DisplaySettingsViewModel);
            switch (args?.PropertyName)
            {
                case nameof(DisplaySettingsViewModel.PaceLaps):
                    PaceLapsChanged();
                    break;
                case nameof(SessionOptionsViewModel.OrderingMode):
                    ChangeOrderingMode();
                    break;
                case nameof(SessionOptionsViewModel.TimesDisplayMode):
                    ChangeTimeDisplayMode();
                    break;
            }
        }

        private void ApplyDisplaySettings(DisplaySettingsViewModel settingsView)
        {
            _trackInfoViewModel.TemperatureUnits = settingsView.TemperatureUnits;
            _trackInfoViewModel.DistanceUnits = settingsView.DistanceUnits;
        }

        private void DisplaySettingsChanged()
        {
            DisplaySettingsViewModel newDisplaySettingsViewModel = _displaySettingsViewModel;
            newDisplaySettingsViewModel.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.PracticeSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.RaceSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.QualificationSessionDisplayOptionsView.PropertyChanged += OnDisplaySettingsChange;
            newDisplaySettingsViewModel.LayoutSettingsViewModel.PropertyChanged += LayoutSettingsViewModelOnPropertyChanged;
        }

        private void LayoutSettingsViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LayoutSettingsViewModel.LayoutDescription))
            {
                CreateMainViewModel();
            }
        }

        private static async Task SchedulePeriodicAction(Action action, Func<int> delayFunc, TimingApplicationViewModel sender, bool captureContext)
        {
            while (!sender.TerminatePeriodicTasks)
            {
                await Task.Delay(delayFunc(), CancellationToken.None).ConfigureAwait(captureContext);

                if (!sender.TerminatePeriodicTasks)
                {
                    action();
                }
            }
        }

        private void SessionEventProviderOnPlayerFinishStateChanged(object sender, DataSetArgs e)
        {
            if (e.DataSet.SessionInfo.SessionType != SessionType.Race || e.DataSet.PlayerInfo.FinishStatus != DriverFinishStatus.Finished || _sessionTiming?.WasGreen != true)
            {
                return;
            }

            PlayerFinished?.Invoke(this, new SessionSummaryEventArgs(_sessionTiming.ToSessionSummary(true)));
        }

        private void CheckAndNotifySessionCompleted()
        {
            if (_sessionTiming?.WasGreen != true || _sessionTiming.IsFinished)
            {
                return;
            }

            _sessionTiming.Finish();
            SessionCompleted?.Invoke(this, new SessionSummaryEventArgs(_sessionTiming.ToSessionSummary(false)));
        }

        private void SituationOverviewControllerOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(SituationOverviewController.SituationOverviewViewModel))
            {
                NotifyPropertyChanged(nameof(SituationOverviewController));
            }
        }

        private void CreateMainViewModel()
        {
            try
            {
                MainViewModel = _layoutFactory.Create(_displaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription.RootElement, _viewModelsCache);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to use stored layout, using default layout.", "Error when creating layout.", MessageBoxButton.OK, MessageBoxImage.Information);
                Logger.Error(ex,"Unable to create layout, creating default");
                var defaultLayout = _displaySettingsViewModel.GetDefaultLayout();
                MainViewModel = _layoutFactory.Create(defaultLayout.RootElement, _viewModelsCache);
                _displaySettingsViewModel.LayoutSettingsViewModel.LayoutDescription = defaultLayout;
            }
        }
    }
}
