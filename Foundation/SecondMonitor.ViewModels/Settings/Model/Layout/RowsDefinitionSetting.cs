﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    using System;

    [Serializable]
    public class RowsDefinitionSetting : GenericContentSetting
    {
        public int RowCount { get; set; }
        public LengthDefinitionSetting[] RowsSize;
        public GenericContentSetting[] RowsContent;
    }
}