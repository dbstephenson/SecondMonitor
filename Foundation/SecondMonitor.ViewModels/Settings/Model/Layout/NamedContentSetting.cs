﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class NamedContentSetting : GenericContentSetting
    {
        [XmlAttribute]
        public string ContentName { get; set; }
    }
}