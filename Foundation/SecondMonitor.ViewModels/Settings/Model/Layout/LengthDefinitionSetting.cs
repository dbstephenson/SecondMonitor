﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    using System;

    [Serializable]
    public class LengthDefinitionSetting
    {
        public LengthDefinitionSetting()
        {
            SizeKind = SizeKind.Remaining;
            ManualSize = 100;
        }

        public LengthDefinitionSetting(SizeKind sizeKind, int manualSize)
        {
            SizeKind = sizeKind;
            ManualSize = manualSize;
        }

        public SizeKind SizeKind { get; set; }

        public int ManualSize { get; set; }
    }
}