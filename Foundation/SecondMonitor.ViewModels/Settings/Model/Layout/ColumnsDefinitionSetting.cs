﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class ColumnsDefinitionSetting : GenericContentSetting
    {
        [XmlAttribute]
        public int ColumnsCount { get; set; }

        public LengthDefinitionSetting[] ColumnsSize { get; set; }

        public GenericContentSetting[] ColumnsContent { get; set; }
    }
}