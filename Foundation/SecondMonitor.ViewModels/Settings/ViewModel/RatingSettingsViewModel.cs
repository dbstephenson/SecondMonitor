﻿namespace SecondMonitor.ViewModels.Settings.ViewModel
{
    using Model;

    public class RatingSettingsViewModel : AbstractViewModel<RatingSettings>
    {
        private bool _isEnabled;
        private string _selectedReferenceRatingProvider;
        private string[] _availableReferenceRatingProviders;
        private int _graceLapsCount;
        private double _deviation;
        private int _normalizedFieldSize;

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        public int GraceLapsCount
        {
            get => _graceLapsCount;
            set => SetProperty(ref _graceLapsCount, value);
        }

        public string SelectedReferenceRatingProvider
        {
            get => _selectedReferenceRatingProvider;
            set => SetProperty(ref _selectedReferenceRatingProvider, value);
        }

        public double Deviation
        {
            get => _deviation;
            set => SetProperty(ref _deviation, value);
        }

        public string[] AvailableReferenceRatingProviders
        {
            get => _availableReferenceRatingProviders;
            set => SetProperty(ref _availableReferenceRatingProviders, value);
        }

        public int NormalizedFieldSize
        {
            get => _normalizedFieldSize;
            set => SetProperty(ref _normalizedFieldSize, value);
        }

        protected override void ApplyModel(RatingSettings model)
        {
            IsEnabled = model.IsEnabled;
            SelectedReferenceRatingProvider = model.SelectedReferenceRatingProvider;
            GraceLapsCount = model.GraceLapsCount;
            Deviation = model.Deviation;
            NormalizedFieldSize = model.NormalizedFieldSize;
        }

        public override RatingSettings SaveToNewModel()
        {
            return new RatingSettings()
            {
                IsEnabled = IsEnabled,
                SelectedReferenceRatingProvider = SelectedReferenceRatingProvider,
                GraceLapsCount = GraceLapsCount,
                Deviation = Deviation,
                NormalizedFieldSize = NormalizedFieldSize,
            };
        }
    }
}