﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System.Collections.ObjectModel;
    using Contracts.FuelInformation;

    public class FuelPlannerViewModel : AbstractViewModel
    {
        private ISessionFuelConsumptionViewModel _selectedSession;
        private FuelCalculatorViewModel _calculatorForSelectedSession;
        private bool _isVisible;

        public FuelPlannerViewModel()
        {
            IsVisible = false;
            Sessions = new ObservableCollection<ISessionFuelConsumptionViewModel>();
            CalculatorForSelectedSession = new FuelCalculatorViewModel();
        }

        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        public ObservableCollection<ISessionFuelConsumptionViewModel> Sessions { get; }
        public ISessionFuelConsumptionViewModel SelectedSession
        {
            get => _selectedSession;
            set
            {
                _selectedSession = value;
                UpdateCalculatorViewModel();
                NotifyPropertyChanged();
            }
        }

        public FuelCalculatorViewModel CalculatorForSelectedSession
        {
            get => _calculatorForSelectedSession;
            set
            {
                _calculatorForSelectedSession = value;
                NotifyPropertyChanged();
            }
        }

        private void UpdateCalculatorViewModel()
        {
            CalculatorForSelectedSession.FuelConsumption = SelectedSession.FuelConsumption;
            CalculatorForSelectedSession.LapDistance = SelectedSession.LapDistance.InMeters;
        }
    }
}