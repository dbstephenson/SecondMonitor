﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using Settings;

    public class PedalsAndGearViewModel : AbstractViewModel, ISimulatorDataSetViewModel
    {
        public const string ViewModelLayoutName = "Pedals And Gear";
        private readonly ISettingsProvider _settingsProvider;

        public PedalsAndGearViewModel(ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        public double ThrottlePercentage { get; set; }

        public Velocity Speed { get; private set; }

        public int Rpm { get; private set; }

        public double WheelRotation { get; set; }

        public double ClutchPercentage { get; set; }

        public double BrakePercentage { get; set; }

        public double ThrottleFilteredPercentage { get; set; }

        public double BrakeFilteredPercentage { get; set; }

        public string Gear { get; set; }


        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (dataSet?.PlayerInfo?.CarInfo == null || dataSet.InputInfo == null || !_settingsProvider.DisplaySettingsViewModel.EnablePedalInformation)
            {
                return;
            }

            if (dataSet.InputInfo.ThrottlePedalPosition >= 0)
            {
                ThrottlePercentage = Math.Round(dataSet.InputInfo.ThrottlePedalPosition * 100, 1);
                //ThrottleFilteredPercentage = dataSet.InputInfo.ThrottlePedalFilteredPosition * 100;
            }

            if (dataSet.InputInfo.BrakePedalPosition >= 0)
            {
                BrakePercentage = Math.Round(dataSet.InputInfo.BrakePedalPosition * 100, 1);
                //BrakeFilteredPercentage = dataSet.InputInfo.BrakePedalFilteredPosition * 100;
            }

            if (dataSet.InputInfo.ClutchPedalPosition >= 0)
            {
                ClutchPercentage = Math.Round(dataSet.InputInfo.ClutchPedalPosition * 100, 1);
            }

            Speed = dataSet.PlayerInfo.Speed;
            Rpm = dataSet.PlayerInfo.CarInfo.EngineRpm;


            WheelRotation = Math.Round(dataSet.InputInfo.WheelAngle, 1);
            Gear = dataSet.PlayerInfo.CarInfo.CurrentGear;
            NotifyPropertyChanged(string.Empty);
        }

        public void Reset()
        {

        }
    }
}