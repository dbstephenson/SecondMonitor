﻿namespace SecondMonitor.ViewModels.Layouts.Factory
{
    using Settings.Model.Layout;

    public interface IDefaultLayoutFactory
    {
        LayoutDescription CreateDefaultLayout();
    }
}