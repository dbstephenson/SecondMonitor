﻿namespace SecondMonitor.ViewModels.Layouts.Factory
{
    using OxyPlot;

    public interface IViewModelByNameFactory
    {
        IViewModel Create(string viewModelName);
    }
}