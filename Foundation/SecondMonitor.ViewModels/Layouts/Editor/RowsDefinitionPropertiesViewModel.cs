﻿namespace SecondMonitor.ViewModels.Layouts.Editor
{
    using System.Windows.Input;
    using Settings.Model.Layout;

    public class RowsDefinitionPropertiesViewModel : AbstractViewModel<RowsDefinitionSetting>
    {
        private ICommand _removeCommand;
        private ICommand _addNewRowCommand;

        public RowsDefinitionPropertiesViewModel()
        {
            GenericSettingsPropertiesViewModel = new GenericSettingsPropertiesViewModel();
        }
        
        public GenericSettingsPropertiesViewModel GenericSettingsPropertiesViewModel { get; }

        public ICommand RemoveCommand
        {
            get => _removeCommand;
            set => SetProperty(ref _removeCommand, value);
        }

        public ICommand AddNewRowCommand
        {
            get => _addNewRowCommand;
            set => SetProperty(ref _addNewRowCommand, value);
        }
        

        protected override void ApplyModel(RowsDefinitionSetting model)
        {
            GenericSettingsPropertiesViewModel.FromModel(model);
        }

        public override RowsDefinitionSetting SaveToNewModel()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyGenericSettings(RowsDefinitionSetting newModel)
        {
            GenericSettingsPropertiesViewModel.ApplyGenericSettings(newModel);
        }
    }
}