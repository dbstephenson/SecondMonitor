﻿namespace SecondMonitor.ViewModels.Layouts.Editor
{
    using System.Windows.Input;
    using Settings.Model.Layout;

    public class ColumnsDefinitionPropertiesViewModel : AbstractViewModel<ColumnsDefinitionSetting>
    {
        private ICommand _removeCommand;
        private ICommand _addNewColumnCommand;

        public ColumnsDefinitionPropertiesViewModel()
        {
            GenericSettingsPropertiesViewModel = new GenericSettingsPropertiesViewModel();
        }
        
        public GenericSettingsPropertiesViewModel GenericSettingsPropertiesViewModel { get; }

        public ICommand RemoveCommand
        {
            get => _removeCommand;
            set => SetProperty(ref _removeCommand, value);
        }
        
        public ICommand AddNewColumnCommand
        {
            get => _addNewColumnCommand;
            set => SetProperty(ref _addNewColumnCommand, value);
        }

        protected override void ApplyModel(ColumnsDefinitionSetting model)
        {
            GenericSettingsPropertiesViewModel.FromModel(model);
        }

        public override ColumnsDefinitionSetting SaveToNewModel()
        {
            throw new System.NotImplementedException();
        }

        public void ApplyGenericSettings(ColumnsDefinitionSetting newModel)
        {
            GenericSettingsPropertiesViewModel.ApplyGenericSettings(newModel);
        }
    }
}