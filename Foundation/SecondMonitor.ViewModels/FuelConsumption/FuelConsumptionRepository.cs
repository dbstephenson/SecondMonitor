﻿namespace SecondMonitor.ViewModels.FuelConsumption
{
    using DataModel.Summary.FuelConsumption;
    using Repository;
    using Settings;

    public class FuelConsumptionRepository : AbstractXmlRepository<OverallFuelConsumptionHistory>
    {

        public FuelConsumptionRepository(ISettingsProvider settingsProvider)
        {
            RepositoryDirectory = settingsProvider.SimulatorContentRepository;
        }

        protected override string RepositoryDirectory { get; }

        protected override string FileName => "FuelConsumption.xml";
    }
}