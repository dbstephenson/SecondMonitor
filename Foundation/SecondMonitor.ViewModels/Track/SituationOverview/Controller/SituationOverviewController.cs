﻿namespace SecondMonitor.ViewModels.Track.SituationOverview.Controller
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Contracts.Commands;
    using Contracts.Session;
    using Contracts.SimSettings;
    using Contracts.TrackMap;
    using Controllers;
    using DataModel.BasicProperties;
    using DataModel.DriversPresentation;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using DataModel.TrackMap;
    using NLog;
    using SessionEvents;
    using Settings;
    using Settings.ViewModel;
    using SituationOverview;
    using Track;

    public class SituationOverviewController : IController, INotifyPropertyChanged
    {
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly SituationOverviewViewModelFactory _situationOverviewViewModelFactory;
        private readonly IDriverPresentationsManager _driverPresentationsManager;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private ISituationOverviewViewModel _situationOverviewViewModel;
        private readonly IMapManagementController _mapManagementController;
        private readonly ISessionInformationProvider _sessionInformationProvider;
        private (string fullTrackName, string simName) _currentTrackTuple;
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;
        private bool _usePositionInClass;


        public SituationOverviewController(ISettingsProvider settingsProvider , ISessionEventProvider sessionEventProvider, SituationOverviewViewModelFactory situationOverviewViewModelFactory,
            IDriverPresentationsManager driverPresentationsManager, IMapManagementController mapManagementController, ISessionInformationProvider sessionInformationProvider)
        {
            _sessionEventProvider = sessionEventProvider;
            _situationOverviewViewModelFactory = situationOverviewViewModelFactory;
            _driverPresentationsManager = driverPresentationsManager;
            _mapManagementController = mapManagementController;
            _sessionInformationProvider = sessionInformationProvider;
            SubscribeMapManager();
            driverPresentationsManager.DriverCustomColorChanged += DriverPresentationsManagerOnDriverCustomColorChanged;
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            _sessionEventProvider.TrackChanged += SessionEventProviderOnTrackChanged;
        }

        public ISituationOverviewViewModel SituationOverviewViewModel
        {
            get => _situationOverviewViewModel;
            private set
            {
                _situationOverviewViewModel = value;
                NotifyPropertyChanged();
            }
        }

        public Task StartControllerAsync()
        {
            SubscribeDisplaySettings();
            SubscribeMapManager();
            SubscribeDisplaySettings();
            ApplyDisplaySettings();
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            UnSubscribeDisplaySettings();
            UnsubscribeMapManager();
            UnSubscribeDisplaySettings();
            return Task.CompletedTask;
        }

        private void ApplyDisplaySettings()
        {
            if (_displaySettingsViewModel == null )
            {
                return;
            }

            _usePositionInClass = _displaySettingsViewModel.MultiClassDisplayKind == MultiClassDisplayKind.ClassFirst || _displaySettingsViewModel.MultiClassDisplayKind == MultiClassDisplayKind.OnlyClass;

            if (SituationOverviewViewModel != null)
            {
                SituationOverviewViewModel.AnimateDrivers = _displaySettingsViewModel.AnimateDriversPosition;
                SituationOverviewViewModel.DriversUpdatedPerTick = _displaySettingsViewModel.DriversUpdatedPerTick;
            }

            if (_mapManagementController != null)
            {
                _mapManagementController.MapPointsInterval = TimeSpan.FromMilliseconds(_displaySettingsViewModel.MapDisplaySettingsViewModel.MapPointsInterval);
            }
        }

        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (dataSet == null || SituationOverviewViewModel == null)
            {
                return;
            }

            var unknownDrivers = SituationOverviewViewModel.Update(dataSet, _sessionInformationProvider, _usePositionInClass);
            unknownDrivers.ForEach(AddDriver);
        }

        public void Reset()
        { ;
            _situationOverviewViewModel?.RemoveAllDrivers();
        }

        public void RemoveDriver(DriverInfo driver)
        {
            SituationOverviewViewModel?.RemoveDriver(driver);
        }

        public void AddDriver(DriverInfo driver)
        {
            if (_driverPresentationsManager.TryGetDriverPresentation(driver.DriverLongName, out DriverPresentationDto driverPresentationDto) && driverPresentationDto.CustomOutLineEnabled)
            {
                SituationOverviewViewModel?.AddDriver(driver, driverPresentationDto.OutLineColor);
            }
            else
            {
                SituationOverviewViewModel?.AddDriver(driver);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SubscribeMapManager()
        {
            if (_mapManagementController == null)
            {
                return;
            }

            _mapManagementController.NewMapAvailable += OnNewMapAvailable;
            _mapManagementController.MapRemoved += OnMapRemoved;
        }

        private void UnsubscribeMapManager()
        {
            if (_mapManagementController == null)
            {
                return;
            }

            _mapManagementController.NewMapAvailable -= OnNewMapAvailable;
            _mapManagementController.MapRemoved -= OnMapRemoved;
        }

        private void SubscribeDisplaySettings()
        {
            if (_displaySettingsViewModel == null)
            {
                return;
            }

            _displaySettingsViewModel.PropertyChanged += OnDisplaySettingsChanged;
            _displaySettingsViewModel.MapDisplaySettingsViewModel.PropertyChanged += OnDisplaySettingsChanged;
        }

        private void UnSubscribeDisplaySettings()
        {
            if (_displaySettingsViewModel == null)
            {
                return;
            }

            _displaySettingsViewModel.PropertyChanged -= OnDisplaySettingsChanged;
            _displaySettingsViewModel.MapDisplaySettingsViewModel.PropertyChanged -= OnDisplaySettingsChanged;
        }

        private void OnDisplaySettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            ApplyDisplaySettings();
        }


        private void LoadCurrentMap(SimulatorDataSet dataSet)
        {
            if (dataSet == null)
            {
                return;
            }

            _currentTrackTuple = (dataSet.SessionInfo.TrackInfo.TrackFullName, dataSet.Source);
            if (string.IsNullOrEmpty(_currentTrackTuple.fullTrackName) || _mapManagementController == null)
            {
                return;
            }

            Logger.Info("Loading New Map");
            if(_displaySettingsViewModel.MapDisplaySettingsViewModel.AlwaysUseCirce || !_mapManagementController.TryGetMap(_currentTrackTuple.simName, _currentTrackTuple.fullTrackName, out TrackMapDto trackMapDto))
            {
                SituationOverviewViewModel = _situationOverviewViewModelFactory.CreateDefault(dataSet);
            }
            else
            {
                SituationOverviewViewModel = _situationOverviewViewModelFactory.Create(dataSet, trackMapDto);
            }

            dataSet.DriversInfo.ForEach(AddDriver);

            SituationOverviewViewModel.MapSidePanelViewModel.DeleteMapCommand = new RelayCommand(RemoveCurrentMap);
            SituationOverviewViewModel.MapSidePanelViewModel.RotateMapLeftCommand = new RelayCommand(RotateCurrentMapLeft);
            SituationOverviewViewModel.MapSidePanelViewModel.RotateMapRightCommand = new RelayCommand(RotateCurrentMapRight);

        }

        private void SessionEventProviderOnTrackChanged(object sender, DataSetArgs e)
        {
            LoadCurrentMap(e.DataSet);
        }

        private void OnNewMapAvailable(object sender, MapEventArgs e)
        {
            if (_currentTrackTuple.simName == e.TrackMapDto.SimulatorSource && _currentTrackTuple.fullTrackName == e.TrackMapDto.TrackFullName)
            {
                Logger.Info($"New Map is Available: {_currentTrackTuple.simName}, {_currentTrackTuple.fullTrackName}");
                LoadCurrentMap(_sessionEventProvider.LastDataSet);
            }
        }

        private void OnMapRemoved(object sender, MapEventArgs e)
        {
            if (_currentTrackTuple.simName == e.TrackMapDto.SimulatorSource && _currentTrackTuple.fullTrackName == e.TrackMapDto.TrackFullName)
            {
                Logger.Info($" Map Removed: {_currentTrackTuple.simName}, {_currentTrackTuple.fullTrackName}");
                LoadCurrentMap(_sessionEventProvider.LastDataSet);
            }
        }

        private void RemoveCurrentMap()
        {
            _mapManagementController.RemoveMap(_currentTrackTuple.simName, _currentTrackTuple.fullTrackName);
        }

        private void RotateCurrentMapLeft()
        {
            _mapManagementController.RotateMapLeft(_currentTrackTuple.simName, _currentTrackTuple.fullTrackName);
        }

        private void RotateCurrentMapRight()
        {
             _mapManagementController.RotateMapRight(_currentTrackTuple.simName, _currentTrackTuple.fullTrackName);
        }

        private void DriverPresentationsManagerOnDriverCustomColorChanged(object sender, DriverCustomColorEnabledArgs e)
        {
            if (e.IsCustomOutlineEnabled)
            {
                SituationOverviewViewModel?.UpdateCustomOutline(e.DriverLongName, e.DriverColor);
            }
            else
            {
                SituationOverviewViewModel?.UpdateCustomOutline(e.DriverLongName, null);
            }
        }
    }
}