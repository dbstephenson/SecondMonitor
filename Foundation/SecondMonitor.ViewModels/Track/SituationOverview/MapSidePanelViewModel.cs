﻿namespace SecondMonitor.ViewModels.Track.SituationOverview
{
    using System.Windows.Input;
    using ViewModels;

    public class MapSidePanelViewModel : AbstractViewModel, IMapSidePanelViewModel
    {
        private ICommand _deleteMapCommand;
        private ICommand _rotateMapLeftCommand;
        private ICommand _rotateMapRightCommand;

        public ICommand DeleteMapCommand
        {
            get => _deleteMapCommand;
            set => SetProperty(ref _deleteMapCommand, value);
        }

        public ICommand RotateMapLeftCommand
        {
            get => _rotateMapLeftCommand;
            set => SetProperty(ref _rotateMapLeftCommand, value);
        }

        public ICommand RotateMapRightCommand
        {
            get => _rotateMapRightCommand;
            set => SetProperty(ref _rotateMapRightCommand, value);
        }
    }
}