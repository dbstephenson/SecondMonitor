﻿namespace SecondMonitor.PluginManager.SimulatorSettings
{
    using Contracts.NInject;
    using Contracts.SimSettings;
    using Ninject;
    using Ninject.Planning.Bindings;
    using Ninject.Syntax;

    public class SimSettingsFactory : ISimSettingsFactory
    {
        private readonly IResolutionRoot _resolutionRoot;

        public SimSettingsFactory(IResolutionRoot resolutionRoot)
        {
            _resolutionRoot = resolutionRoot;
        }

        public ISimSettings Create(string simulatorName)
        {
            if (string.IsNullOrWhiteSpace(simulatorName))
            {
                return new DefaultSimulatorSettings();
            }

            try
            {
                return _resolutionRoot.Get<ISimSettings>(bm => WhenMetadataMatched(bm, simulatorName));
            }
            catch (ActivationException)
            {
                return new DefaultSimulatorSettings();
            }
        }

        private bool WhenMetadataMatched(IBindingMetadata metadata, string simulatorName)
        {
            if (string.IsNullOrWhiteSpace(simulatorName))
            {
                return !metadata.Has(BindingMetadataIds.SimulatorNameBinding);
            }

            if (!metadata.Has(BindingMetadataIds.SimulatorNameBinding))
            {
                return false;
            }

            return metadata.Get<string>(BindingMetadataIds.SimulatorNameBinding) == simulatorName;
        }
    }
}