﻿namespace SecondMonitor.DataModel.Snapshot
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class PitWindowInformation
    {
        [XmlAttribute]
        public PitWindowState PitWindowState { get; set; }

        [XmlAttribute]
        public double PitWindowStart { get; set; }

        [XmlAttribute]
        public double PitWindowEnd { get; set; }
    }
}