﻿namespace SecondMonitor.DataModel
{
    using System;

    public static class SimulatorsNameMap
    {
        public const string NotConnected = "Not connected";

        public const string R3ESimName = "R3E";
        public const string ACSimName = "Assetto Corsa";
        public const string ACCSimName = "ACC";
        public const string PCars2SimName = "PCars 2";
        public const string AMS2SimName = "AMS 2";
        public const string AMSSimName = "AMS";
        public const string RF2SimName = "RFactor 2";

        public static bool IsR3ESimulator(string simulatorName)
        {
            return simulatorName.Equals(R3ESimName, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsNotConnected(string simulatorName)
        {
            return simulatorName.Equals(NotConnected, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}