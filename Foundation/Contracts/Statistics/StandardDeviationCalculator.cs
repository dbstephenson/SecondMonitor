﻿namespace SecondMonitor.Contracts.Statistics
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class StandardDeviationCalculator
    {
        public static StandardDeviationResult<double> ComputeDeviation(ICollection<double> values)
        {
            double mean = values.Sum() / values.Count;

            var squaresQuery = values.Select(x => (x - mean) * (x - mean));
            double sumOfSquares = squaresQuery.Sum();

            double stdDeviation = Math.Sqrt(sumOfSquares / values.Count());

            return new StandardDeviationResult<double>(mean, stdDeviation);
        }
    }
}