﻿namespace SecondMonitor.Contracts.SimSettings
{
    using System;
    using DataModel.BasicProperties;

    public class DriverCustomColorEnabledArgs : EventArgs
    {
        public DriverCustomColorEnabledArgs(string driverLongName, bool isCustomOutlineEnabled, ColorDto driverColor)
        {
            DriverLongName = driverLongName;
            IsCustomOutlineEnabled = isCustomOutlineEnabled;
            DriverColor = driverColor;
        }

        public string DriverLongName { get; }
        public bool IsCustomOutlineEnabled { get; }

        public ColorDto DriverColor { get; }
    }
}