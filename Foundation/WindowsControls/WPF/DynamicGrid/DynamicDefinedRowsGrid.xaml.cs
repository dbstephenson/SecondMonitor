﻿using System.Windows.Controls;

namespace SecondMonitor.WindowsControls.WPF.DynamicGrid
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.Linq;
    using System.Windows;
    using Converters;
    using ViewModels;
    using ViewModels.Settings.Model.Layout;

    public partial class DynamicDefinedRowsGrid : UserControl
    {
        public static readonly DependencyProperty ViewModelsProperty =  DependencyProperty.Register("ViewModels", typeof(ICollection<IViewModel>), typeof(DynamicDefinedRowsGrid), new FrameworkPropertyMetadata(OnViewModelsPropertyChanged));
        public static readonly DependencyProperty RowsHeightProperty =  DependencyProperty.Register("RowsHeight", typeof(ICollection<LengthDefinitionSetting>), typeof(DynamicDefinedRowsGrid), new FrameworkPropertyMetadata(OnViewModelsPropertyChanged));
        private readonly SizeDefinitionToSizeConverter _sizeConverter;
        public DynamicDefinedRowsGrid()
        {
            InitializeComponent();
            _sizeConverter = new SizeDefinitionToSizeConverter();
        }

        public ICollection<LengthDefinitionSetting> RowsHeight
        {
            get => (ICollection<LengthDefinitionSetting>) GetValue(RowsHeightProperty);
            set => SetValue(RowsHeightProperty, value);
        }

        public ICollection<IViewModel> ViewModels
        {
            get => (ICollection<IViewModel>) GetValue(ViewModelsProperty);
            set => SetValue(ViewModelsProperty, value);
        }

        private void RefreshMainGrid()
        {
            if (ViewModels == null || RowsHeight == null)
            {
                MainGrid.Children.Clear();
                return;
            }

            List<IViewModel> viewModels = ViewModels.ToList();
            List<LengthDefinitionSetting> rowsHeight = RowsHeight.ToList();
            int numberOfRows = rowsHeight.Count;

            if (numberOfRows == 0)
            {
                MainGrid.Children.Clear();
                return;
            }

            MainGrid.Children.Clear();
            MainGrid.RowDefinitions.Clear();

            for (int i = 0; i < numberOfRows; i++)
            {
                RowDefinition rowDefinition = new RowDefinition()
                {
                    Height = (GridLength) _sizeConverter.Convert(rowsHeight[i], typeof(GridLength), null, CultureInfo.InvariantCulture)
                };
                MainGrid.RowDefinitions.Add(rowDefinition);

                ContentPresenter contentPresenter = new ContentPresenter()
                {
                    Content = viewModels[i],
                };
                Grid.SetRow(contentPresenter, i);
                MainGrid.Children.Add(contentPresenter);
            }
        }

        private void TryUnsubscribe(ICollection<IViewModel> viewModels)
        {
            if (viewModels is ObservableCollection<IViewModel> observableCollection)
            {
                observableCollection.CollectionChanged -= ObservableCollectionOnCollectionChanged;
            }
        }

        private void TrySubscribe(ICollection<IViewModel> viewModels)
        {
            if (viewModels is ObservableCollection<IViewModel> observableCollection)
            {
                observableCollection.CollectionChanged += ObservableCollectionOnCollectionChanged;
            }
        }

        private void ObservableCollectionOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            RefreshMainGrid();
        }

        private static void OnViewModelsPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DynamicDefinedRowsGrid dynamicRowsGrid)
            {
                dynamicRowsGrid.TryUnsubscribe(e.OldValue as ICollection<IViewModel>);
                dynamicRowsGrid.TrySubscribe(e.NewValue as ICollection<IViewModel>);
                dynamicRowsGrid.RefreshMainGrid();
            }
        }
    }
}