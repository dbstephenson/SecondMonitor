﻿using System.Windows.Controls;

namespace SecondMonitor.WindowsControls.WPF.DriverPosition
{
    /// <summary>
    /// Interaction logic for MapSidePanelControl.xaml
    /// </summary>
    public partial class MapSidePanelControl : UserControl
    {
        public MapSidePanelControl()
        {
            InitializeComponent();
        }
    }
}
