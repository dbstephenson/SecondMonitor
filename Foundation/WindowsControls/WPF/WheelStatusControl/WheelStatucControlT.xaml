﻿<ResourceDictionary xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                    xmlns:behaviors="clr-namespace:SecondMonitor.WindowsControls.WPF.Behaviors"
                    xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"
                    xmlns:quantityText="clr-namespace:SecondMonitor.WindowsControls.WPF.QuantityText"
                    xmlns:wpf="clr-namespace:SecondMonitor.WindowsControls.WPF"
                    xmlns:volumeBehavior="clr-namespace:SecondMonitor.WindowsControls.WPF.Behaviors.VolumeBehavior"
                    xmlns:tyreWear="clr-namespace:SecondMonitor.WindowsControls.WPF.Behaviors.TyreWear"
                    xmlns:converters="clr-namespace:SecondMonitor.WindowsControls.WPF.Converters"
                    xmlns:carStatus="clr-namespace:SecondMonitor.ViewModels.CarStatus;assembly=SecondMonitor.ViewModels">
    <BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
    <converters:InvertedDoubleConverter x:Key="InvertedDoubleConverter" />
    <converters:InverseBooleanToVisibilityConverter x:Key="InverseBooleanToVisibilityConverter" />

    <DataTemplate DataType="{x:Type carStatus:CarWheelsViewModel}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="*" />
                <ColumnDefinition Width="*" />
            </Grid.ColumnDefinitions>
            <Grid.RowDefinitions>
                <RowDefinition Height="*" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>
            <ContentPresenter Content="{Binding LeftFrontTyre}" Grid.Row="0" Grid.Column="0"/>
            <ContentPresenter Content="{Binding RightFrontTyre}" Grid.Row="0" Grid.Column="1" />
            <ContentPresenter Content="{Binding LeftRearTyre}" Grid.Row="1" Grid.Column="0"/>
            <ContentPresenter Content="{Binding RightRearTyre}" Grid.Row="1" Grid.Column="1"/>
        </Grid>

    </DataTemplate>

    <DataTemplate DataType="{x:Type carStatus:WheelStatusViewModel}">
        <Border BorderBrush="{StaticResource DarkGrey01Brush}" BorderThickness="1" CornerRadius="2" Margin="1">
            <Grid Background="{StaticResource Anthracite01Brush}" MinWidth="200">
                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto" />
                        <RowDefinition Height="*" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <Grid Row="0">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>

                        <quantityText:TemperatureText Grid.Column="0" Margin="5,0,0,0" HorizontalAlignment="Left"
                                                      VerticalAlignment="Top"
                                                      FontSize="{StaticResource BrakeTemperatureFontSize}"
                                                      FontWeight="Bold"
                                                      Foreground="{StaticResource LightGrey01Brush}"
                                                      Quantity="{Binding TyreLeftTemperature.ActualQuantity, Mode=OneWay}"
                                                      TemperatureUnits="{Binding TemperatureUnits, Mode=OneWay}">
                            <i:Interaction.Behaviors>
                                <volumeBehavior:ForegroundByTemperatureBehavior
                                    DefaultColor="{StaticResource OptimalQuantityColor}"
                                    HighQuantityColor="{StaticResource HighQuantityColor}"
                                    IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                    LowQuantityColor="{StaticResource LowQuantityColor}"
                                    Volume="{Binding TyreLeftTemperature, Mode=OneWay}" />
                            </i:Interaction.Behaviors>
                        </quantityText:TemperatureText>

                        <quantityText:TemperatureText Grid.Column="1" HorizontalAlignment="Center"
                                                      VerticalAlignment="Top"
                                                      FontSize="{StaticResource BrakeTemperatureFontSize}"
                                                      FontWeight="Bold"
                                                      Foreground="{StaticResource LightGrey01Brush}"
                                                      Quantity="{Binding TyreCenterTemperature.ActualQuantity, Mode=OneWay}"
                                                      TemperatureUnits="{Binding TemperatureUnits, Mode=OneWay}">
                            <i:Interaction.Behaviors>
                                <volumeBehavior:ForegroundByTemperatureBehavior
                                    DefaultColor="{StaticResource OptimalQuantityColor}"
                                    HighQuantityColor="{StaticResource HighQuantityColor}"
                                    IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                    LowQuantityColor="{StaticResource LowQuantityColor}"
                                    Volume="{Binding TyreCenterTemperature, Mode=OneWay}" />
                            </i:Interaction.Behaviors>
                        </quantityText:TemperatureText>

                        <quantityText:TemperatureText Grid.Column="2" Margin="0,0,5,0" HorizontalAlignment="Right"
                                                      VerticalAlignment="Top"
                                                      FontSize="{StaticResource BrakeTemperatureFontSize}"
                                                      FontWeight="Bold"
                                                      Foreground="{StaticResource LightGrey01Brush}"
                                                      Quantity="{Binding TyreRightTemperature.ActualQuantity, Mode=OneWay}"
                                                      TemperatureUnits="{Binding TemperatureUnits, Mode=OneWay}">
                            <i:Interaction.Behaviors>
                                <volumeBehavior:ForegroundByTemperatureBehavior
                                    DefaultColor="{StaticResource OptimalQuantityColor}"
                                    HighQuantityColor="{StaticResource HighQuantityColor}"
                                    IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                    LowQuantityColor="{StaticResource LowQuantityColor}"
                                    Volume="{Binding TyreRightTemperature, Mode=OneWay}" />
                            </i:Interaction.Behaviors>
                        </quantityText:TemperatureText>

                    </Grid>
                    <Grid Row="1" >
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="Auto" />
                        </Grid.ColumnDefinitions>
                        <Grid Grid.Column="0" Margin="5,0,5,0" Visibility="{Binding IsLeftWheel, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}}">
                            <Grid Background="{StaticResource TyreSlidingBrush}" Height="10" Panel.ZIndex="1"  Visibility="{Binding IsSliding, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}}"/>
                            <wpf:ColorAbleIcon StrokeBrush="{StaticResource Green01Brush}" Width="23"
                                               RenderTransformOrigin="0.5, 0.5">
                                <ContentPresenter Content="{StaticResource TyreIcon}" />
                                <i:Interaction.Behaviors>
                                    <tyreWear:ColorAbleIconByLimits IdealLimitColor="{StaticResource Green01Color}"
                                                                    MildLimitColor="{StaticResource Yellow01Color}"
                                                                    HeavyLimitColor="{StaticResource LightRed01Color}"
                                                                    Value="{Binding TyreCondition}"
                                                                    IdealLimit="{Binding TyreNoWearWearLimit}"
                                                                    MildLimit="{Binding TyreMildWearLimit}"
                                                                    HeavyLimit="{Binding TyreHeavyWearLimit}" />

                                </i:Interaction.Behaviors>
                                <wpf:ColorAbleIcon.RenderTransform>
                                    <RotateTransform
                                        Angle="{Binding WheelCamber, Converter={StaticResource InvertedDoubleConverter}}" />
                                </wpf:ColorAbleIcon.RenderTransform>
                            </wpf:ColorAbleIcon>
                        </Grid>
                        <Border x:Name="StatusBorder" Grid.Column="1" Margin="0,0,0,0" HorizontalAlignment="Stretch"
                                VerticalAlignment="Stretch"
                                BorderBrush="{StaticResource LightGrey02Brush}"
                                BorderThickness="4" CornerRadius="4">
                            <Grid x:Name="StatusGrid" Margin="0,0,0,0" HorizontalAlignment="Stretch"
                                  VerticalAlignment="Stretch"
                                  Background="{StaticResource Anthracite01Brush}">
                                <Grid HorizontalAlignment="Left" VerticalAlignment="Stretch" Background="DarkGreen">
                                    <i:Interaction.Behaviors>
                                        <volumeBehavior:BackgroundByTemperatureBehavior
                                            DefaultColor="{StaticResource DarkGrey01Color}"
                                            HighQuantityColor="{StaticResource HighQuantityColor}"
                                            IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                            LowQuantityColor="{StaticResource LowQuantityColor}"
                                            Volume="{Binding TyreCoreTemperature, Mode=OneWay}" />
                                        <behaviors:PercentagesWidthBehavior
                                            Percentage="{Binding TyreCondition, Mode=OneWay}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                                <Grid Width="1" Margin="0,0,0,0" VerticalAlignment="Stretch" HorizontalAlignment="Left"
                                      Background="{StaticResource DarkRed01Brush}" Grid.ZIndex="1"
                                      ToolTip="Heavy wear limit">
                                    <i:Interaction.Behaviors>
                                        <behaviors:LeftMarginPercentageBehavior
                                            Percentage="{Binding TyreHeavyWearLimit, Mode=OneWay}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                                <Grid Width="1" Margin="10,0,0,0" VerticalAlignment="Stretch"
                                      HorizontalAlignment="Left" Background="{StaticResource Yellow01Brush}"
                                      Grid.ZIndex="1">
                                    <i:Interaction.Behaviors>
                                        <behaviors:LeftMarginPercentageBehavior
                                            Percentage="{Binding TyreMildWearLimit, Mode=OneWay}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                                <Grid Width="4" Margin="0,0,0,0" VerticalAlignment="Stretch" HorizontalAlignment="Left"
                                      Background="{StaticResource Anthracite05Brush}" Grid.ZIndex="1" ToolTip="Predicted wear at race end.">
                                    <i:Interaction.Behaviors>
                                        <behaviors:LeftMarginPercentageBehavior
                                            Percentage="{Binding WearAtRaceEnd, Mode=OneWay}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                                <Grid Width="4" Margin="0,0,0,0" VerticalAlignment="Stretch" HorizontalAlignment="Left"
                                      Background="{StaticResource TyreWearAtRaceEndBrush}" Grid.ZIndex="1" ToolTip="Predicted wear at stint end.">
                                    <i:Interaction.Behaviors>
                                        <behaviors:LeftMarginPercentageBehavior
                                            Percentage="{Binding WearAtStintEnd, Mode=OneWay}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                                <Grid Grid.ZIndex="1">
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="Auto" />
                                        <ColumnDefinition Width="Auto" />
                                        <ColumnDefinition Width="*" />
                                        <ColumnDefinition Width="Auto" />
                                    </Grid.ColumnDefinitions>
                                    <TextBlock Grid.Column="0" Style="{StaticResource SmallText}"
                                               Text="{Binding TyreCompound}" HorizontalAlignment="Left"
                                               VerticalAlignment="Center" Grid.ZIndex="3" />
                                    <TextBlock Grid.Column="1" Margin="5,0,0,0"
                                               ToolTip="Predicted laps until tyre will be heavily worn."
                                               Style="{StaticResource SmallText}"
                                               Text="{Binding LapsUntilHeavyWear, Mode=OneWay}"
                                               Visibility="{Binding LapsUntilHeavyWear, Mode=OneWay, Converter={StaticResource IntToVisibilityConverter}}"
                                               HorizontalAlignment="Left" VerticalAlignment="Center" Grid.ZIndex="3" />

                                    <quantityText:TemperatureText Grid.Column="2" HorizontalAlignment="Center"
                                                                  VerticalAlignment="Center"
                                                                  FontSize="{StaticResource WheelControlCoreTempFontSize}"
                                                                  Foreground="{StaticResource LightGrey01Brush}"
                                                                  Quantity="{Binding TyreCoreTemperature.ActualQuantity, Mode=OneWay}"
                                                                  TemperatureUnits="{Binding TemperatureUnits, Mode=OneWay}"
                                                                  Grid.ZIndex="3" />
                                    <TextBlock Grid.Column="3" Style="{StaticResource SmallText}" Margin="5"
                                               HorizontalAlignment="Center" VerticalAlignment="Center" Grid.ZIndex="3"
                                               Text="{Binding TyreCondition, Mode=OneWay, Converter={StaticResource DoubleToStringNoDecimalConverter}}" />

                                </Grid>
                            </Grid>
                        </Border>
                        <Grid Grid.Column="2" Margin="5,0,5,0" Visibility="{Binding IsLeftWheel, Mode=OneWay, Converter={StaticResource InverseBooleanToVisibilityConverter}}">
                            <Grid Background="{StaticResource TyreSlidingBrush}" Height="10" Panel.ZIndex="1"  Visibility="{Binding IsSliding, Mode=OneWay, Converter={StaticResource BooleanToVisibilityConverter}}"/>
                            <wpf:ColorAbleIcon StrokeBrush="{StaticResource Green01Brush}" Width="23"
                                               RenderTransformOrigin="0.5, 0.5">
                                <ContentPresenter Content="{StaticResource TyreIcon}" />

                                <i:Interaction.Behaviors>
                                    <tyreWear:ColorAbleIconByLimits IdealLimitColor="{StaticResource Green01Color}"
                                                                    MildLimitColor="{StaticResource Yellow01Color}"
                                                                    HeavyLimitColor="{StaticResource LightRed01Color}"
                                                                    Value="{Binding TyreCondition}"
                                                                    IdealLimit="{Binding TyreNoWearWearLimit}"
                                                                    MildLimit="{Binding TyreMildWearLimit}"
                                                                    HeavyLimit="{Binding TyreHeavyWearLimit}" />

                                </i:Interaction.Behaviors>
                                <wpf:ColorAbleIcon.RenderTransform>
                                    <RotateTransform Angle="{Binding WheelCamber}" />
                                </wpf:ColorAbleIcon.RenderTransform>
                            </wpf:ColorAbleIcon>
                        </Grid>
                    </Grid>
                    <Grid MaxHeight="23" Margin="0,4,0,2" Row="2">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="Auto" />
                        </Grid.ColumnDefinitions>
                        <Border Width="5" VerticalAlignment="Stretch" Background="{StaticResource Anthracite05Brush}" Grid.Column="0"
                                HorizontalAlignment="Left" Margin="2,0,2,0" ToolTip="Brake Condition"
                                BorderThickness="1" BorderBrush="{StaticResource DarkGrey05Brush}">
                            <Grid VerticalAlignment="Stretch" HorizontalAlignment="Stretch">
                                <Grid VerticalAlignment="Bottom" Background="Green">
                                    <i:Interaction.Behaviors>
                                        <behaviors:PercentagesHeightBehavior
                                            Percentage="{Binding BrakeCondition}" />
                                    </i:Interaction.Behaviors>
                                </Grid>
                            </Grid>
                        </Border>
                        <ContentPresenter Grid.Column="1" Margin="0,0,10,0"
                                          Content="{StaticResource BrakeIcon}" />
                        <quantityText:TemperatureText Grid.Column="2" HorizontalAlignment="Center"
                                                      VerticalAlignment="Top" Margin="0,0,20,0"
                                                      FontSize="{StaticResource BrakeTemperatureFontSize}"
                                                      FontWeight="Bold"
                                                      Foreground="{StaticResource LightGrey01Brush}"
                                                      Quantity="{Binding BrakeTemperature.ActualQuantity, Mode=OneWay}"
                                                      TemperatureUnits="{Binding TemperatureUnits, Mode=OneWay}">
                            <i:Interaction.Behaviors>
                                <volumeBehavior:ForegroundByTemperatureBehavior
                                    DefaultColor="{StaticResource OptimalQuantityColor}"
                                    HighQuantityColor="{StaticResource HighQuantityColor}"
                                    IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                    LowQuantityColor="{StaticResource LowQuantityColor}"
                                    Volume="{Binding BrakeTemperature, Mode=OneWay}" />
                            </i:Interaction.Behaviors>
                        </quantityText:TemperatureText>

                        <quantityText:PressureText Grid.Column="4" HorizontalAlignment="Center" VerticalAlignment="Top"
                                                   FontSize="{StaticResource BrakeTemperatureFontSize}"
                                                   FontWeight="Bold"
                                                   Foreground="{StaticResource LightGrey01Brush}"
                                                   PressureUnits="{Binding PressureUnits, Mode=OneWay}"
                                                   Quantity="{Binding TyrePressure.ActualQuantity, Mode=OneWay}">
                            <i:Interaction.Behaviors>
                                <volumeBehavior:ForegroundByPressureBehavior
                                    DefaultColor="{StaticResource LightGrey01Color}"
                                    HighQuantityColor="{StaticResource HighQuantityColor}"
                                    IdealQuantityColor="{StaticResource OptimalQuantityColor}"
                                    LowQuantityColor="{StaticResource LowQuantityColor}"
                                    Volume="{Binding TyrePressure, Mode=OneWay}" />
                            </i:Interaction.Behaviors>
                        </quantityText:PressureText>
                        <ContentPresenter Grid.Column="5" Margin="10,0,5,0"
                                          Content="{StaticResource TyrePressureIcon}" />
                    </Grid>
                </Grid>
            </Grid>
        </Border>
        <DataTemplate.Resources>
            <Storyboard x:Key="FlashRed" RepeatBehavior="Forever">
                <ColorAnimation Storyboard.TargetName="StatusBorder" Storyboard.TargetProperty="BorderBrush.Color"
                                To="{StaticResource LightRed01Color}"
                                Duration="0:0:0.5" />
                <ColorAnimation Storyboard.TargetName="StatusBorder" Storyboard.TargetProperty="BorderBrush.Color"
                                To="{StaticResource LightGrey02Color}"
                                Duration="0:0:.5" />
            </Storyboard>

            <Storyboard x:Key="FlashRedGrid" RepeatBehavior="Forever">
                <ColorAnimation Storyboard.TargetName="StatusGrid" Storyboard.TargetProperty="Background.Color"
                                To="{StaticResource LightRed01Color}"
                                Duration="0:0:0.5" />
                <ColorAnimation Storyboard.TargetName="StatusGrid" Storyboard.TargetProperty="Background.Color"
                                To="{StaticResource Anthracite01Color}"
                                Duration="0:0:.5" />
            </Storyboard>

            <Storyboard x:Key="ReturnToGrey">
                <ColorAnimation Storyboard.TargetName="StatusBorder" Storyboard.TargetProperty="BorderBrush.Color"
                                To="{StaticResource LightGrey02Color}"
                                Duration="0:0:0.5" />
            </Storyboard>

            <Storyboard x:Key="ReturnToGreyGrid">
                <ColorAnimation Storyboard.TargetName="StatusGrid" Storyboard.TargetProperty="Background.Color"
                                To="{StaticResource Anthracite01Color}"
                                Duration="0:0:0.5" />
            </Storyboard>
        </DataTemplate.Resources>
        <DataTemplate.Triggers>
            <DataTrigger Binding="{Binding IsTyreDetached}" Value="True">
                <DataTrigger.EnterActions>
                    <BeginStoryboard x:Name="FlashRedStart" Storyboard="{StaticResource FlashRed}" />
                    <BeginStoryboard x:Name="FlashRedGridStart" Storyboard="{StaticResource FlashRedGrid}" />
                </DataTrigger.EnterActions>
                <DataTrigger.ExitActions>
                    <StopStoryboard BeginStoryboardName="FlashRedStart" />
                    <BeginStoryboard Storyboard="{StaticResource ReturnToGrey}" />
                    <StopStoryboard BeginStoryboardName="FlashRedGridStart" />
                    <BeginStoryboard Storyboard="{StaticResource ReturnToGreyGrid}" />
                </DataTrigger.ExitActions>
            </DataTrigger>
        </DataTemplate.Triggers>
    </DataTemplate>
</ResourceDictionary>