﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.Synchronization.Graphs
{
    using System;

    public class DistanceEventArgs : EventArgs
    {
        public DistanceEventArgs(double distance)
        {
            Distance = distance;
        }

        public double Distance { get; }
    }
}