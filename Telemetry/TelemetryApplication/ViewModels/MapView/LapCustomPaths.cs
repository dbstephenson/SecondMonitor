﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.MapView
{
    using System.Collections.Generic;
    using System.Linq;
    using SecondMonitor.ViewModels.Shapes;

    public class LapCustomPathsCollection : ILapCustomPathsCollection
    {
        private readonly Dictionary<double, AbstractShapeViewModel> _brakingPaths;
        private readonly Dictionary<double, AbstractShapeViewModel> _throttlePaths;
        private readonly Dictionary<double, AbstractShapeViewModel> _clutchPaths;

        public LapCustomPathsCollection()
        {
            _brakingPaths = new Dictionary<double, AbstractShapeViewModel>();
            _throttlePaths = new Dictionary<double, AbstractShapeViewModel>();
            _clutchPaths = new Dictionary<double, AbstractShapeViewModel>();
        }

        public string LapId { get; set; }

        public bool FullyInitialized { get; set; }

        public AbstractShapeViewModel ShiftPointsPath { get; set; }

        public AbstractShapeViewModel BaseLapPath { get; set; }

        public void AddBrakingPath(AbstractShapeViewModel path, double intensity)
        {
            _brakingPaths[intensity] = path;
        }

        public AbstractShapeViewModel GetBrakingPath(double intensity)
        {
            return _brakingPaths[intensity];
        }

        public IEnumerable<AbstractShapeViewModel> GetAllBrakingPaths() => _brakingPaths.Values;


        public void AddClutchPath(AbstractShapeViewModel path, double intensity)
        {
            _clutchPaths[intensity] = path;
        }

        public IEnumerable<AbstractShapeViewModel> GetAllClutchPaths() => _clutchPaths.Values;


        public void AddThrottlePath(AbstractShapeViewModel path, double intensity)
        {
            _throttlePaths[intensity] = path;
        }

        public AbstractShapeViewModel GetThrottlePath(double intensity)
        {
            return _throttlePaths[intensity];
        }

        public IEnumerable<AbstractShapeViewModel> GetAllThrottlePaths() => _throttlePaths.Values;


        public AbstractShapeViewModel[] GetAllPaths()
        {
            return _brakingPaths.Values.Concat(_throttlePaths.Values.Concat(_clutchPaths.Values).Append(BaseLapPath).Append(ShiftPointsPath)).ToArray();
        }
    }
}