﻿namespace SecondMonitor.AMS2Connector
{
    using System.Collections.Generic;
    using System.Linq;
    using Contracts.NInject;
    using Ninject.Modules;

    public class Ams2ConnectorModuleBootstrapper : INinjectModuleBootstrapper
    {
        public IList<INinjectModule> GetModules()
        {
            return new INinjectModule[] {new Ams2ConnectorModule(), }.ToList();
        }
    }
}