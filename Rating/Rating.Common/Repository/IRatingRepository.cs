﻿namespace SecondMonitor.Rating.Common.Repository
{
    using DataModel;

    public interface IRatingRepository
    {
        Ratings SimulatorsRatings { get; }

        void SaveRatings(Ratings ratings);
    }
}