﻿namespace SecondMonitor.Rating.Common.Championship.Calendar.Tracks
{
    using DataModel.Championship;

    public static class TracksTemplates
    {
        public static TrackTemplate AdelaideGp8595 => new TrackTemplate("Adelaide - Grand Prix (1985 - 1995)", 3780);
        public static TrackTemplate AdelaideGpPresent=> new TrackTemplate("Adelaide (Present)", 3219);

        public static TrackTemplate AdriaFull => new TrackTemplate("Adria - Full Circuit", 2702);

        public static TrackTemplate AintreeGrandPrix => new TrackTemplate("Aintree - Grand Prix", 4828);

        public static TrackTemplate AlgarveCircuit1Present => new TrackTemplate("Algarve, Portimão - Circuit 1", 4699);

        public static TrackTemplate Alemannenring => new TrackTemplate("Alemannenring", 2800);

        public static TrackTemplate AlbertParkPresent => new TrackTemplate("Albert Park, Melbourne (Present)", 5303);

        public static TrackTemplate Anderstorp7292 => new TrackTemplate("Anderstorp - Grand Prix (1972 - 1992)", 4025);

        public static TrackTemplate AutodromMost => new TrackTemplate("Autodrom Most", 4222);

        public static TrackTemplate AVUSBerling => new TrackTemplate("AVUS (Berlin)", 8300);

        public static TrackTemplate AutopolisHitaInternational => new TrackTemplate("Autopolis -  International Circuit", 4673);

        public static TrackTemplate BahrainGP => new TrackTemplate("Bahrain International Circuit - Grand Prix", 5411);

        public static TrackTemplate BakuGP => new TrackTemplate("Baku - Grand Prix", 6003);

        public static TrackTemplate BarberMotorsportsPark => new TrackTemplate("Barber Motorsports Park", 3700);

        public static TrackTemplate BathurstPresent => new TrackTemplate("Bathurst Mount Panorama", 6213);

        public static TrackTemplate BangsaenStreetCircuit => new TrackTemplate("Bangsaen - Street Circuit", 3700);

        public static TrackTemplate BarbagalloRacewaySupercars => new TrackTemplate("Barbagallo Raceway Wanneroo - Supercars circuit", 2411);

        public static TrackTemplate BendMotorsportParkInternational => new TrackTemplate("The Bend Motorsport Park - international circuit", 4950);

        public static TrackTemplate BrainerdInternationalRaceway6888 => new TrackTemplate("Brainerd International Raceway (1968 - 1988)", 4989);

        public static TrackTemplate BrandsHatchGpPresent => new TrackTemplate("Brands Hatch - Grand Prix (Present)", 3916);
        public static TrackTemplate BrandsHatchGp7687 => new TrackTemplate("Brands Hatch - Grand Prix (1976 - 1987)", 4207);
        public static TrackTemplate BrandsHatchGp6075 => new TrackTemplate("Brands Hatch - Grand Prix (1960 - 1975)", 4265);
        public static TrackTemplate BrandsHatchIndyPresent => new TrackTemplate("Brands Hatch - Indy (Present)", 1944);

        public static TrackTemplate BrnoPresent => new TrackTemplate("Brno - Grand Prix (Present)", 5403);

        public static TrackTemplate BuenosAiresNo6 => new TrackTemplate("Buenos Aires - Circuit No. 6", 4101);

        public static TrackTemplate BuenosAiresNo9 => new TrackTemplate("Buenos Aires - Circuit No. 9", 3346);

        public static TrackTemplate BudhGp => new TrackTemplate("Buddh International Circuit - Grand Prix", 5137);

        public static TrackTemplate CanadianTireMotosportPark => new TrackTemplate("Canadian Tire Motorsport Park", 4102);

        public static TrackTemplate CampoGrande => new TrackTemplate("Campo Grande", 3443);

        public static TrackTemplate Cascavel => new TrackTemplate("Cascavel", 3302);

        public static TrackTemplate ChangGp => new TrackTemplate("Chang International Circuit - Full Circuit", 4554);

        public static TrackTemplate CircuitDeCatalunyaGpPresent => new TrackTemplate("Circuit de Catalunya - Grand Prix (Present)", 4655);
        public static TrackTemplate CircuitDeCatalunyaGp9194 => new TrackTemplate("Circuit de Catalunya - Grand Prix (1991 - 1994)", 4747);
        public static TrackTemplate CircuitDeCatalunyaGp9503 => new TrackTemplate("Circuit de Catalunya - Grand Prix (1995 - 2003)", 4730);

        public static TrackTemplate CircuitdelaSarthe9194 => new TrackTemplate("Circuit des 24 Heures du Mans (1991 - 1996)", 13600);
        public static TrackTemplate CircuitdelaSarthe7985 => new TrackTemplate("Circuit des 24 Heures du Mans (1979 - 1985)", 13626);

        public static TrackTemplate CircuitdelaSarthePresent => new TrackTemplate("Circuit des 24 Heures du Mans", 13629);

        public static TrackTemplate CircuitMontTremblantFull => new TrackTemplate("Mont-Tremblant - Full Course", 4260);

        public static TrackTemplate ColumbusStreetCourse => new TrackTemplate("Columbus street course	", 4023);

        public static TrackTemplate CotaGP => new TrackTemplate("Circuit of the Americas - Grand Prix", 5515);

        public static TrackTemplate CroftMainCircuit => new TrackTemplate("Croft - Main Circuit", 3423);

        public static TrackTemplate CharadeGrandPrix5888 => new TrackTemplate("Charade - Grand Prix (1958 - 1988)", 8055);

        public static TrackTemplate GoldenportMororPark => new TrackTemplate("Goldenport Motor Park - Main Course", 2390);

        public static TrackTemplate Goiania => new TrackTemplate("Goiânia", 3835);


        public static TrackTemplate DaytonaRoadCourse => new TrackTemplate("Daytona - Road Course", 5729);
        public static TrackTemplate DaytonaRoadCourse7583 => new TrackTemplate("Daytona - Road Course (1975 - 1983)", 6180);
        public static TrackTemplate DaytonaRoadCourse8502 => new TrackTemplate("Daytona - Road Course (1985 - 2002)", 5729);
        public static TrackTemplate DaytonaOval => new TrackTemplate("Daytona - Speedway", 4023);

        public static TrackTemplate DetroitBelleIsle => new TrackTemplate("Detroit Belle Isle", 3798);

        public static TrackTemplate DelMarFairgrounds => new TrackTemplate("Del Mar Raceway", 2600);

        public static TrackTemplate DijonPrenoisGPPresent => new TrackTemplate("Dijon-Prenois - Grand Prix", 3886);
        public static TrackTemplate DijonPrenoisGP7275 => new TrackTemplate("Dijon-Prenois - Grand Prix (1971 - 1975)", 3886);


        public static TrackTemplate DoningtonPark => new TrackTemplate("Donington Park - Grand Prix", 4020);

        public static TrackTemplate DoningtonParkNational => new TrackTemplate("Donington Park - National", 3149);
        public static TrackTemplate DoningtonParkGP8609 => new TrackTemplate("Donington Park - Grand Prix (1986 - 2009)", 4020);

        public static TrackTemplate DiepholzAirfieldCircuit => new TrackTemplate("Diepholz Airfield Circuit", 2720);

        public static TrackTemplate EstorilGp94to99 => new TrackTemplate("Estoril - Grand Prix (1994 - 1999)", 4360);
        public static TrackTemplate EstorilGp72to93 => new TrackTemplate("Estoril - Grand Prix (1972 - 1993)", 4350);


        public static TrackTemplate FujiGpPresent => new TrackTemplate("Fuji Speedway - Grand Prix", 4549);
        public static TrackTemplate FujiGp8485 => new TrackTemplate("Fuji Speedway - Grand Prix (1984 - 1985)", 4410);
        public static TrackTemplate FujiGp7483 => new TrackTemplate("Fuji Speedway - Grand Prix (1974 - 1983)", 4410);


        public static TrackTemplate Hallett => new TrackTemplate("Hallett Motor Racing Circuit", 2897);

        public static TrackTemplate HelsinkyThunder => new TrackTemplate("Helsinki Thunder - Street Circuit", 3301);

        public static TrackTemplate HiddenValleyRaceway => new TrackTemplate("Hidden Valley Raceway", 2870);

        public static TrackTemplate HockenheimringGp7080 => new TrackTemplate("Hockenheimring - Grand Prix (Historic, 1970 - 1980)", 6790);

        public static TrackTemplate HockenheimringGp8291 => new TrackTemplate("Hockenheimring - Grand Prix (Historic, 1982 - 1991)", 6798);
        public static TrackTemplate HockenheimringGp92to01 => new TrackTemplate("Hockenheimring - Grand Prix (Historic, 1992 - 2001)", 6825);
        public static TrackTemplate HockenheimringGpPresent => new TrackTemplate("Hockenheimring - Grand Prix (Present)", 4574);

        public static TrackTemplate HungaroringPresent => new TrackTemplate("Hungaroring (Present)", 4381);
        public static TrackTemplate Hungaroring8902 => new TrackTemplate("Hungaroring (1989 - 2002)", 3968);


        public static TrackTemplate IowaSpeedwayOval => new TrackTemplate("Iowa Speedway - Oval", 1408);

        public static TrackTemplate InterlagosGpPresent => new TrackTemplate("Interlagos - Grand Prix (Present)", 4309);
        public static TrackTemplate InterlagosGp4089 => new TrackTemplate("Interlagos - Grand Prix (1940 - 1989)", 7874);

        public static TrackTemplate ImolaGp8594 => new TrackTemplate("Imola - Grand Prix (1985 - 1994)", 5040);
        public static TrackTemplate ImolaGp9506 => new TrackTemplate("Imola - Grand Prix (1995 - 2006)", 4933);

        public static TrackTemplate ImolaGpPresent => new TrackTemplate("Imola - Grand Prix (Present)", 4909);

        public static TrackTemplate IndianapolisMotorSpeedwayRoadPresent => new TrackTemplate("Indianapolis Motor Speedway - Road Course (Present)", 3925);
        public static TrackTemplate IndianapolisMotorSpeedwayOval => new TrackTemplate("Indianapolis Motor Speedway - Oval", 4023);

        public static TrackTemplate IndianapolisRacewayPark => new TrackTemplate("Indianapolis Raceway Park - Road Course", 4000);

        public static TrackTemplate IstanbulParkGp => new TrackTemplate("Istanbul Park - Grand Prix", 5333);

        public static TrackTemplate JaramaGp6679 => new TrackTemplate("Jarama - Grand Prix (1966 - 1979)", 3404);
        public static TrackTemplate JaramaGpPresent => new TrackTemplate("Jarama - Grand Prix (1966 - 1979)", 3850);

        public static TrackTemplate JerezGpPresent => new TrackTemplate("Jerez - Grand Prix (Present)", 4429);

        public static TrackTemplate LagunaSecaPresent => new TrackTemplate("Laguna Seca", 3602);
        public static TrackTemplate LagunaSeca6885 => new TrackTemplate("Laguna Seca (1968 - 1985)", 3058);
        public static TrackTemplate LagunaSeca8687 => new TrackTemplate("Laguna Seca (1986 - 1987)", 3058);
        public static TrackTemplate LagunaSeca9095 => new TrackTemplate("Laguna Seca (1990 - 1995)", 3563);

        public static TrackTemplate LasVegasCombinedLayout => new TrackTemplate("Las Vegas Motor Speedway - Combined Layout");

        public static TrackTemplate LongBeach => new TrackTemplate("Long Beach", 3167);

        public static TrackTemplate Londrina => new TrackTemplate("Londrina", 3145);

        public static TrackTemplate LausitzringGpPresent => new TrackTemplate("EuroSpeedway Lausitzring - Grand Prix (Present)", 4345);
        public static TrackTemplate LausitzringDtmShortCoursePresent => new TrackTemplate("EuroSpeedway Lausitzring - DTM Short course(Present)", 3478);

        public static TrackTemplate LeMansBugatti => new TrackTemplate("Le Mans Bugatti Circuit", 4430);

        public static TrackTemplate LimeRockRoadCourse => new TrackTemplate("Lime Rock Park - Road Course", 2462);


        public static TrackTemplate KasselCaldenAirfield => new TrackTemplate("Kassel-Calden Airfield", 2591);

        public static TrackTemplate KnockhillNational => new TrackTemplate("Knockhill Racing Circuit - National", 1609);

        public static TrackTemplate KoreaGp => new TrackTemplate("Korea International Circuit - Grand Prix", 5615);

        public static TrackTemplate KyalamiGPPresent => new TrackTemplate("Kyalami - Grand Prix (Present)", 4580);

        public static TrackTemplate KyalamiGP9308 => new TrackTemplate("Kyalami - Grand Prix (1993 - 2008)", 4260);

        public static TrackTemplate KyalamiGP6187 => new TrackTemplate("Kyalami - Grand Prix (1961 - 1987)", 4104);


        public static TrackTemplate MadrasIrungattukottaiFull => new TrackTemplate(" Madras Motor Sports Club - Full Circuit", 3700);

        public static TrackTemplate MagnyCourseGp92to02 => new TrackTemplate("Magny-Cours - Grand Prix (1992 - 2002)", 4250);

        public static TrackTemplate MainzFinthenAirport => new TrackTemplate("Mainz-Finthen Airport", 2250);

        public static TrackTemplate MacauPresent => new TrackTemplate("Macau (Present)", 6115);

        public static TrackTemplate MarrakechPresent => new TrackTemplate("Marrakesh (Present)", 3000);

        public static TrackTemplate MexicoGpPresent => new TrackTemplate("Autódromo Hermanos Rodríguez - Grand Prix (Present)", 4580);

        public static TrackTemplate MexicoGp5985 => new TrackTemplate("Autódromo Hermanos Rodríguez - Pista 1 (1959 - 1985)", 5000);

        public static TrackTemplate MidOhioMainWithoutChicane => new TrackTemplate("Mid-Ohio - Main Circuit, without chicane", 3621);
        public static TrackTemplate MidOhio6389 => new TrackTemplate("Mid-Ohio - Sports Car Course (1963 - 1988)", 3862);

        public static TrackTemplate MisanoWorldCircuitPresent => new TrackTemplate("Misano - World Circuit (Present)", 4200);
        public static TrackTemplate MisanoWorldCircuit7282 => new TrackTemplate("Misano - World Circuit (1972 - 1982)", 3488);

        public static TrackTemplate MonacoPresent => new TrackTemplate("Circuit de Monaco (Present)", 3337);
        public static TrackTemplate Monaco8696 => new TrackTemplate("Circuit de Monaco (1986 - 1996)", 3328);
        public static TrackTemplate Monaco9702 => new TrackTemplate("Circuit de Monaco (1997 - 2002)", 3367);

        public static TrackTemplate Monaco5571 => new TrackTemplate("Circuit de Monaco (1955 - 1971)", 3145);
        public static TrackTemplate Monaco72 => new TrackTemplate("Circuit de Monaco (1972)", 3145);
        public static TrackTemplate Monaco7685 => new TrackTemplate("Circuit de Monaco (1976 - 1985)", 3312);

        public static TrackTemplate MontrealGpPresent => new TrackTemplate("Circuit Gilles Villenueve - Grand Prix (Present)", 4361);
        public static TrackTemplate MontrealGp9193 => new TrackTemplate("Circuit Gilles Villenueve - Grand Prix (1991 - 1993)", 4430);
        public static TrackTemplate MontrealGp9601 => new TrackTemplate("Circuit Gilles Villenueve - Grand Prix (1996 - 2001)", 4421);

        public static TrackTemplate MonzaGpPresent => new TrackTemplate("Monza - Present", 5793);
        public static TrackTemplate MonzaGp7694 => new TrackTemplate("Monza (1976 - 1994)", 5800);
        public static TrackTemplate MonzaGp9599 => new TrackTemplate("Monza (1995 - 1999)", 5770);

        public static TrackTemplate MonzaCombined5965 => new TrackTemplate("Monza Combined (1959 - 1965)", 6214);
        public static TrackTemplate MonzaRoad5971 => new TrackTemplate("Monza Road Cource (1959 - 1971)", 6214);
        public static TrackTemplate MonzaRoad7275 => new TrackTemplate("Monza Road Cource (1972 - 1975)", 3588);

        public static TrackTemplate MotorlandAragonGrandPrix => new TrackTemplate("Motorland Aragón - Grand Prix", 5344);

        public static TrackTemplate MoscowRacewayFim => new TrackTemplate("Moscow Raceway - Grand Prix FIM (Present)", 3931);

        public static TrackTemplate MugelloGp7490 => new TrackTemplate("Mugello - Grand Prix (1974 - 1990)", 5245);
        public static TrackTemplate MugelloGpPresent => new TrackTemplate("Mugello - Grand Prix (Present)", 5245);

        public static TrackTemplate NavarraSpeedCircuitLong => new TrackTemplate("Navarra - Speed Circuit Long", 3933);

        public static TrackTemplate NewcastleStreetCircuit => new TrackTemplate("Newcastle street circuit", 2641);

        public static TrackTemplate NingboPresent => new TrackTemplate("Ningbo International Speedway", 4015);

        public static TrackTemplate NivellesBaulers => new TrackTemplate("Nivelles-Baulers", 3720);


        public static TrackTemplate NogaroGrandPrixPresent => new TrackTemplate("Circuit Paul Armagnac (Nogaro) - Grand Prix (Present)", 3636);

        public static TrackTemplate NorisringPresent => new TrackTemplate("Norisring (Present)", 2300);

        public static TrackTemplate NordschleifeWithGPNoArena => new TrackTemplate("Nordschleife with GP, no Arena", 25378);
        public static TrackTemplate NordschleifeHistoric => new TrackTemplate("Nordschleife (Historic)", 22834);
        public static TrackTemplate NordschleifeWithGp8302 => new TrackTemplate("Nordschleife with GP (1983 - 2002)", 25359);
        public static TrackTemplate NordschleifeBetonschleife => new TrackTemplate("Nürburgring - Betonschleife (1973 - 1982)", 2292);


        public static TrackTemplate NurburgringGp84to02 => new TrackTemplate("Nürburgring - Grand Prix (1984 - 2004, no Arena)", 4551);
        public static TrackTemplate NurburgringGpPresent => new TrackTemplate("Nürburgring - Grand Prix (Present)", 5137);
        public static TrackTemplate NurburgringSprintPresent => new TrackTemplate("Nürburgring - Sprint (Present)", 3619);

        public static TrackTemplate Oschersleben => new TrackTemplate("Oschersleben - Full Course", 3696);
        public static TrackTemplate Oschersleben00to06 => new TrackTemplate("Oschersleben - Full Course (2000 - 2006)", 3668);

        public static TrackTemplate OkayamaGpPresent => new TrackTemplate("Okayama - Grand Prix", 3702);

        public static TrackTemplate OultonParkIslandCircuit => new TrackTemplate("Oulton Park - Island Circuit", 3637);

        public static TrackTemplate OultonParkInternationalCircuit => new TrackTemplate("Oulton Park - International Circuit", 4333);

        public static TrackTemplate OrdosGrandPrix => new TrackTemplate("Ordos International Circuit - Grand Prix Circuit", 3751);


        public static TrackTemplate PaulRicard1CV2 => new TrackTemplate("Paul Ricard - Circuit 1C-V2", 5842);

        public static TrackTemplate PaulRicard1AV2 => new TrackTemplate("Paul Ricard - Circuit 1A-V2 (No Chicane)", 5770);
        public static TrackTemplate PaulRicardGp7001 => new TrackTemplate("Paul Ricard - Grand Prix (1970 - 2001)", 5809);

        public static TrackTemplate PalmBeach6400 => new TrackTemplate("Palm Beach International Raceway (1964 - 2000)", 3621);

        public static TrackTemplate PhillipIslandGp => new TrackTemplate("Phillip Island - Grand Prix", 4450);

        public static TrackTemplate PittsburghRaceComplexFull => new TrackTemplate("Pittsburgh International Race Complex - Full Track", 4470);

        public static TrackTemplate PoconoRacewayOval => new TrackTemplate("Pocono Raceway - Oval", 4023);

        public static TrackTemplate PortlandInternationalRacewayPresent => new TrackTemplate("Portland International Raceway (Present)", 3166);
        public static TrackTemplate PortlandInternationalRaceway7183 => new TrackTemplate("Portland International Raceway (1971 - 1983)", 3082);
        public static TrackTemplate PortlandInternationalRaceway8491 => new TrackTemplate("Portland International Raceway (1984 - 1991)", 3093);
        public static TrackTemplate PortlandInternationalRaceway9204 => new TrackTemplate("Portland International Raceway (1992 - 2004)", 3166);

        public static TrackTemplate PhoenixInternationalRaceway => new TrackTemplate("Phoenix International Raceway (2011 - 2018)", 1645);
        public static TrackTemplate PhoenixInternationalRacewayRoad9111 => new TrackTemplate("Phoenix International Raceway - Road Course (1991 - 2011)", 1510);

        public static TrackTemplate PotrerodelosFunesPresent => new TrackTemplate("Potrero de los Funes (Present)", 6270);

        public static TrackTemplate PukekohePark => new TrackTemplate("Pukekohe Park", 2910);

        public static TrackTemplate PrinceGeorge => new TrackTemplate("Prince George (East London)", 3920);


        public static TrackTemplate QueenslandRaceway => new TrackTemplate("Queenslands Raceways - National Circuit", 3126);

        public static TrackTemplate RedBullRing => new TrackTemplate("Red Bull Ring - Grand Prix", 4318);
        public static TrackTemplate RedBullA1Ring => new TrackTemplate("A1 Ring - Grand Prix (1996 - 2004)", 4326);
        public static TrackTemplate RedBullOsterreichring => new TrackTemplate("Österreichring - Grand Prix (1969 - 1975)", 4326);

        public static TrackTemplate RiversideInternationalRaceway => new TrackTemplate("Riverside International Raceway", 5300);

        public static TrackTemplate RoadAtlanta => new TrackTemplate("Road Atlanta - Grand Prix", 4088);
        public static TrackTemplate RoadAtlanta7097 => new TrackTemplate("Road Atlanta - Grand Prix (1970 - 1997)", 4055);

        public static TrackTemplate RoadAmerica => new TrackTemplate("Road America", 6514);

        public static TrackTemplate RouenLesEssarts => new TrackTemplate("Rouen-Les-Essarts", 6542);

        public static TrackTemplate SantaCruzdoSul => new TrackTemplate("Santa Cruz do Sul", 3531);

        public static TrackTemplate SanAntonioStreetCourse => new TrackTemplate("San Antonio street course");

        public static TrackTemplate SandownRaceway => new TrackTemplate("Sandown Raceway", 3104);

        public static TrackTemplate Salzburgring7685 => new TrackTemplate("Salzburgring (1976 - 1985)", 4246);

        public static TrackTemplate ShanghaiGp => new TrackTemplate("Shanghai International Circuit - Grand Prix", 5451);

        public static TrackTemplate SachsenringPresent => new TrackTemplate("Sachsenring (Present)", 3671);

        public static TrackTemplate SebringGpPresent => new TrackTemplate("Sebring - Grand Prix (Present)", 5954);
        public static TrackTemplate SebringGp6782 => new TrackTemplate("Sebring - Grand Prix (1967 - 1982)", 8368);
        public static TrackTemplate SebringGp8790 => new TrackTemplate("Sebring - Grand Prix (1987 - 1990)", 6614);
        public static TrackTemplate SebringGp9195 => new TrackTemplate("Sebring - Grand Prix (1991 - 1995)", 5954);

        public static TrackTemplate SepangGPPresent => new TrackTemplate("Sepang - Grand Prix", 5543);

        public static TrackTemplate ShahAlamCircuit => new TrackTemplate("Shah Alam Circuit", 3690);


        public static TrackTemplate SilverstoneGpPresent => new TrackTemplate("Silverstone - Grand Prix (Present)", 5890);
        public static TrackTemplate SilverstoneGp96 => new TrackTemplate("Silverstone - Grand Prix (1996)", 5072);
        public static TrackTemplate SilverstoneGp9193 => new TrackTemplate("Silverstone - Grand Prix (1991 - 1993)", 5226);
        public static TrackTemplate SilverstoneGp9702 => new TrackTemplate("Silverstone - Grand Prix (1997 - 2002)", 5141);
        public static TrackTemplate SilverstoneGp7586 => new TrackTemplate("Silverstone - Grand Prix (1975 - 1986)", 4718);

        public static TrackTemplate SilverstoneGp5274 => new TrackTemplate("Silverstone - Grand Prix (1952 - 1974)", 4710);

        public static TrackTemplate SilverstoneNationalPresent => new TrackTemplate("Silverstone - National (Present)", 2639);

        public static TrackTemplate Singapore => new TrackTemplate("Singapore Marina Bay", 5065);

        public static TrackTemplate SlovakiaRingTrack4 => new TrackTemplate("Slovakia Ring - Layout 4 (Present)", 5922);

        public static TrackTemplate SochipGp => new TrackTemplate("Sochi - Grand Prix", 5872);

        public static TrackTemplate SonomaRacewayIndy => new TrackTemplate("Sonoma Raceway - IndyCar Circuit", 3838);

        public static TrackTemplate SonomaRacewayLong => new TrackTemplate("Sonoma Raceway - Long Circuit", 4056);
        public static TrackTemplate SonomaRaceway6892 => new TrackTemplate("Sonoma Raceway - Long Circuit (1968 - 1992)", 4060);

        public static TrackTemplate Snetterton300 => new TrackTemplate("Snetterton 300", 4779);

        public static TrackTemplate SpaPresent => new TrackTemplate("Spa-Francorchamps (2007 - Present)", 7004);
        public static TrackTemplate Spa8393 => new TrackTemplate("Spa-Francorchamps (1983 - 1993)", 6940);
        public static TrackTemplate Spa9501 => new TrackTemplate("Spa-Francorchamps (1995 - 2001)", 6968);

        public static TrackTemplate SpaHistoric => new TrackTemplate("Spa-Francorchamps (Historic)", 14500);

        public static TrackTemplate StretsOfMiami8704 => new TrackTemplate("Steets of Miami (1987 - 1994)",3014);

        public static TrackTemplate SuzukaGPPresent => new TrackTemplate("Suzuka - Grand Prix (Present)", 5807);
        public static TrackTemplate SuzukaGP9199 => new TrackTemplate("Suzuka - Grand Prix (1991 - 1999)", 5864);
        public static TrackTemplate SuzukaEastPresent => new TrackTemplate("Suzuka - East Circuit (Present)", 2243);

        public static TrackTemplate StPetersburg0316 => new TrackTemplate("St. Petersburg (2003 - 2016)", 2903);
        public static TrackTemplate StPetersburgPresent => new TrackTemplate("St. Petersburg (Present)", 2897);

        public static TrackTemplate SugoInternational => new TrackTemplate("Sugo - International Car Circuit", 3704);

        public static TrackTemplate SummitPointMainCourse => new TrackTemplate("Summit Point - Main Circuit", 3219);

        public static TrackTemplate SurfersParadiseStreetCircuit => new TrackTemplate("Surfers Paradise Street Circuit", 2960);

        public static TrackTemplate SymmonsPlains => new TrackTemplate("Symmons Plains", 2411);


        public static TrackTemplate TexasMotorSpeedwayOval => new TrackTemplate("Texas Motor Speedway, Oval", 2414);

        public static TrackTemplate Thruxton => new TrackTemplate("Thruxton Circuit", 3791);

        public static TrackTemplate TownsvilleStreetCircuit => new TrackTemplate("Townsville street circuit", 2860);

        public static TrackTemplate TorontoExhibitionPlace => new TrackTemplate("Toronto, Exhibition Place", 2824);

        public static TrackTemplate TTAssenGpPresent => new TrackTemplate("TT Circuit Assen - Grand Prix (Present)", 4555);

        public static TrackTemplate TwinRingMotegiRoadCourse => new TrackTemplate("Twin Ring Motegi - Road Course", 4796);


        public static TrackTemplate VallelungaInternationalPresent => new TrackTemplate("Vallelunga - International Circuit (Present)", 4085);
        public static TrackTemplate VallelungaInternational7186 => new TrackTemplate("Vallelunga - International Circuit (1971 - 1986)", 3222);


        public static TrackTemplate VeloCitta => new TrackTemplate("Velo Città", 3493);

        public static TrackTemplate Velopark => new TrackTemplate("Velopark", 2278);



        public static TrackTemplate VilaRealPresent => new TrackTemplate("Vila Real (Present)", 4600);

        public static TrackTemplate VirginiaIntRacewayFull => new TrackTemplate("Virgina International Raceway - Full Course", 5262);


        public static TrackTemplate WatkinsGlenGpWithInnerLoop => new TrackTemplate("Watkins Glen - Grand Prix, with inner loop", 5552);
        public static TrackTemplate WatkinsGlenGpWithoutInnerLoop => new TrackTemplate("Watkins Glen - Grand Prix, without inner loop", 5435);
        public static TrackTemplate WatkinsGlenGp5670 => new TrackTemplate("Watkins Glen - Grand Prix (1956 - 1970)", 3782);
        public static TrackTemplate WatkinsGlenGp7174 => new TrackTemplate("Watkins Glen - Grand Prix (1971 - 1974)", 5435);

        public static TrackTemplate WatkinsGlenGp7583 => new TrackTemplate("Watkins Glen - Grand Prix (1975 - 1983)", 5435);


        public static TrackTemplate WorldWideTechnologyRacewayGatewayOval => new TrackTemplate("World Wide Technology Raceway at Gateway - Oval", 2012);

        public static TrackTemplate WintonMotorRacewayNational => new TrackTemplate("Winton Motor Raceway - National Circuit", 3000);

        public static TrackTemplate WildHorsePassRoadCourse => new TrackTemplate("Wild Horse Pass Motorsports Park");

        public static TrackTemplate WunstorfAirfield => new TrackTemplate("Wunstorf Airfield", 5047);

        public static TrackTemplate WuhanStreetCircuitPresent => new TrackTemplate("Wuhan Street Circuit", 2984);


        public static TrackTemplate YasMarinaGrandPrix => new TrackTemplate("Yas Marina - Grand Prix (Present)", 5554);


        public static TrackTemplate ZandvoortGPPresent => new TrackTemplate("Zandvoort - Grand Prix (Present)", 4307);
        public static TrackTemplate Zandvoort4878 => new TrackTemplate("Zandvoort (1945 - 1978)", 4226);
        public static TrackTemplate Zandvoort79 => new TrackTemplate("Zandvoort (1979)", 4226);


        public static TrackTemplate ZhejiangFullCircuit => new TrackTemplate("Zhejiang Circuit - Full Course", 3200);

        public static TrackTemplate ZhuhaiGp => new TrackTemplate("Zhuhai International Circuit", 4319);

        public static TrackTemplate ZolderGpPresent => new TrackTemplate("Zolder - Grand Prix (Present)", 4010);
        public static TrackTemplate ZolderGp86to93 => new TrackTemplate("Zolder - Grand Prix (1986 - 1993)", 4194);
        public static TrackTemplate ZolderGp7581 => new TrackTemplate("Zolder - Grand Prix (1975 - 1981)", 4262);
    }
}