﻿namespace SecondMonitor.Rating.Application.Championship.Operations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.DataModel.Championship;
    using Common.DataModel.Championship.Events;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using NLog;
    using SecondMonitor.ViewModels.SimulatorContent;

    public class ChampionshipManipulator : IChampionshipManipulator
    {
        private readonly ISimulatorContentProvider _simulatorContentProvider;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ChampionshipManipulator(ISimulatorContentProvider simulatorContentProvider)
        {
            _simulatorContentProvider = simulatorContentProvider;
        }

        public void StartChampionship(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            InitializeDrivers(championship, dataSet);
            championship.GetCurrentOrLastEvent().EventStatus = EventStatus.InProgress;
            championship.ChampionshipState = ChampionshipState.Started;
        }

        public void StartNextEvent(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            var currentEvent = championship.GetCurrentOrLastEvent();
            currentEvent.TrackName = dataSet.SessionInfo.TrackInfo.TrackFullName;
            championship.ClassName = dataSet.PlayerInfo.CarClassName;
        }

        public void AddResultsForCurrentSession(ChampionshipDto championship, SimulatorDataSet dataSet, bool shiftPlayerToLastPlace)
        {
            UpdateAiDriversNames(championship, dataSet);
            var currentEvent = championship.GetCurrentOrLastEvent();
            var currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            //Clear session result so it is not used for drivers comparison
            currentSession.SessionResult = null;
            currentSession.SessionResult = CreateResultDto(championship, dataSet, shiftPlayerToLastPlace);
        }

        public void UpdateAiDriversNames(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            Logger.Info("Championships - Updating Driver Names");
            List<DriverDto> driverPool  = championship.Drivers.Where(x => !x.IsPlayer).ToList();
            List<DriverInfo> driversToAssign = dataSet.GetDriversInfoByMultiClass().Where(x => !x.IsPlayer).ToList();
            //First iteration = use previous names
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                var driverMatch = driverPool.FirstOrDefault(x => x.LastUsedName == driver.DriverLongName);
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driverMatch.LastUsedName} matched");
                driverMatch.LastCarName = driver.CarName;
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            //Second iteration, try find drivers that previously used that name
            foreach (DriverInfo driver in driversToAssign.ToList())
            {
                var driverMatch = driverPool.FirstOrDefault(x => x.OtherNames.Contains(driver.DriverLongName));
                if (driverMatch == null)
                {
                    continue;
                }

                Logger.Info($"Championships - Driver {driver.DriverLongName} replaced {driverMatch.LastUsedName}");
                driverMatch.SetAnotherName(driver.DriverLongName);
                driverMatch.LastCarName = driver.CarName;
                driverPool.Remove(driverMatch);
                driversToAssign.Remove(driver);
            }

            for (int i = 0; i < driverPool.Count; i++)
            {
                Logger.Info($"Championships - Driver {driversToAssign[i].DriverLongName} randomly replaced {driverPool[i].LastUsedName}");
                driverPool[i].LastCarName = driversToAssign[i].CarName;
                driverPool[i].SetAnotherName(driversToAssign[i].DriverLongName);
            }

            UpdateResultsName(championship);
        }

        public void CommitLastSessionResults(ChampionshipDto championship)
        {
            var currentEvent = championship.GetCurrentOrLastEvent();
            var currentSession = currentEvent.Sessions[championship.CurrentSessionIndex];
            var guidDriverDictionary = championship.GetGuidToDriverDictionary();

            if (currentSession.SessionResult == null)
            {
                return;
            }

            foreach (DriverSessionResultDto driverSessionResultDto in currentSession.SessionResult.DriverSessionResult)
            {
                DriverDto driverDto = guidDriverDictionary[driverSessionResultDto.DriverGuid];
                driverDto.TotalPoints = driverSessionResultDto.TotalPoints;
                driverDto.Position = driverSessionResultDto.AfterEventPosition;
                driverDto.LastCarName = driverSessionResultDto.CarName;
            }
            AdvanceChampionship(championship);
        }

        private void UpdateResultsName(ChampionshipDto championship)
        {
            var driverDictionary = championship.GetGuidToDriverDictionary();
            foreach (var driverResult in championship.GetAllResults().SelectMany(x => x.DriverSessionResult))
            {
                driverResult.DriverName = driverDictionary[driverResult.DriverGuid].LastUsedName;
            }
        }

        private void AdvanceChampionship(ChampionshipDto championship)
        {
            var currentEvent = championship.GetCurrentOrLastEvent();
            championship.CurrentSessionIndex = (championship.CurrentSessionIndex + 1) % currentEvent.Sessions.Count;
            if (championship.CurrentSessionIndex == 0)
            {
                currentEvent.EventStatus = EventStatus.Finished;
                championship.CurrentEventIndex++;
                currentEvent = championship.GetCurrentOrLastEvent();

                if (currentEvent.EventStatus == EventStatus.NotStarted)
                {
                    currentEvent.EventStatus = EventStatus.InProgress;
                }
                championship.NextTrack = currentEvent.TrackName;
                ReRollMysteryProperties(championship);
            }

            championship.ChampionshipState = championship.CurrentEventIndex >= championship.Events.Count ? ChampionshipState.Finished : ChampionshipState.Started;
        }

        public void ReRollMysteryProperties(ChampionshipDto championship)
        {
            var currentEvent = championship.GetCurrentOrLastEvent();
            if(currentEvent.IsMysteryTrack)
            {
                currentEvent.TrackName = _simulatorContentProvider.GetRandomTrack(championship.SimulatorName)?.Name ?? string.Empty;
                championship.NextTrack = championship.NextTrack = currentEvent.TrackName;
            }
            if(championship.IsMysteryClass)
            {
                championship.ClassName = _simulatorContentProvider.GetRandomClass(championship.SimulatorName)?.ClassName ?? string.Empty;
            }
        }

        private SessionResultDto CreateResultDto(ChampionshipDto championship, SimulatorDataSet dataSet, bool shiftPlayerToLastPlace)
        {
            var scoring = championship.Scoring[championship.CurrentSessionIndex];
            Dictionary<string, int> positionMap = CreateFinishPositionDictionary(dataSet.GetDriversInfoByMultiClass().ToList(), shiftPlayerToLastPlace);
            SessionResultDto resultDto = new SessionResultDto();
            foreach (DriverDto championshipDriver in championship.Drivers)
            {
                DriverInfo sessionDriver = dataSet.DriversInfo.FirstOrDefault(x => x.DriverLongName == championshipDriver.LastUsedName);
                if (sessionDriver == null)
                {
                    Logger.Error($"Driver {championshipDriver.LastUsedName} not found");
                    throw new InvalidOperationException($"Driver {championshipDriver.LastUsedName} not found");
                }
                int position = positionMap[sessionDriver.DriverLongName];
                DriverSessionResultDto driverResult = new DriverSessionResultDto()
                {
                    DriverGuid = championshipDriver.GlobalKey,
                    DriverName = championshipDriver.LastUsedName,
                    FinishPosition = position,
                    PointsGain = position <= scoring.Scoring.Count ? scoring.Scoring[position - 1] : 0,
                    BeforeEventPosition = championshipDriver.Position,
                    IsPlayer = championshipDriver.IsPlayer,
                    CarName = sessionDriver.CarName,
                };
                driverResult.TotalPoints = championshipDriver.TotalPoints + driverResult.PointsGain;

                resultDto.DriverSessionResult.Add(driverResult);
            }

            DriverSessionResultComparer comparer = new DriverSessionResultComparer(championship, resultDto);
            List<DriverSessionResultDto> driversAfterRaceOrdered = resultDto.DriverSessionResult.OrderBy(x => x, comparer).ToList();
            for (int i = 0; i < driversAfterRaceOrdered.Count; i ++ )
            {
                driversAfterRaceOrdered[i].AfterEventPosition = i + 1;
            }

            return resultDto;
        }

        private static Dictionary<string, int> CreateFinishPositionDictionary(List<DriverInfo> eligibleDrivers, bool playerLast)
        {
            if (!playerLast)
            {
                return eligibleDrivers.ToDictionary(x => x.DriverLongName, x => x.PositionInClass);
            }

            Dictionary<string, int> positionMap = new Dictionary<string, int>();
            List<DriverInfo> driversOrdered = eligibleDrivers.Where(x => !x.IsPlayer).OrderBy(x => x.Position).Concat(eligibleDrivers.Where(x => x.IsPlayer)).ToList();

            for (int i = 0; i < driversOrdered.Count; i++)
            {
                var driver = driversOrdered[i];
                positionMap[driver.DriverLongName] = i + 1;
            }

            return positionMap;
        }

        private void InitializeDrivers(ChampionshipDto championship, SimulatorDataSet dataSet)
        {
            int position = 0;
            IReadOnlyCollection<DriverInfo> eligibleDrivers = dataSet.GetDriversInfoByMultiClass();
            championship.ClassName = dataSet.PlayerInfo.CarClassName;
            championship.TotalDrivers = eligibleDrivers.Count;
            championship.Drivers = eligibleDrivers.Select(x => new DriverDto()
            {
                LastUsedName = x.DriverLongName,
                IsPlayer = x.IsPlayer,
                Position = ++position,
                LastCarName = x.CarName,
            }).ToList();
        }
    }
}